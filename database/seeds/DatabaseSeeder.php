<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        /* DB::table('question_types')->truncate();
        $inputFields2 = [
            [
                'q_type' => 'Read aloud',
                'slug'=>'read-aloud',
                'category'=>'speaking',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'q_type' => 'Repeat sentence',
                'slug'=>'repeat-sentence',
                 'category'=>'speaking',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Describe image',
                'slug'=>'describe-image',
                 'category'=>'speaking',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Re-tell lecture',
                'slug'=>'re-tell-lecture',
                 'category'=>'speaking',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Answer short question',
                'slug'=>'answer-short-question',
                 'category'=>'speaking',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Summarize written text',
                'slug'=>'swt',
                 'category'=>'writting',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Essay (20 mins)',
                'slug'=>'eassy',
                 'category'=>'writting',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Listening: Multiple choice, choose single answer',
                'slug'=>'lmccsa',
                'category'=>'listening',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'q_type' => 'Listening: Multiple choice, choose multiple answers',
                'slug'=>'lmccma',
                 'category'=>'listening',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Reading: Multiple choice, choose single answer',
                'slug'=>'mccsa',
                'category'=>'reading',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'q_type' => 'Reading: Multiple choice, choose multiple answers',
                'slug'=>'mccma',
                'category'=>'reading',
                'created_at' => now(),
                'updated_at' => now(),
                
            ],
            [
                 'q_type' => 'Re-order paragraphs',
                 'slug'=>'rop',
                 'category'=>'reading',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Reading: Fill in the blanks',
                'slug'=>'rfb',
                'category'=>'reading',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'q_type' => 'Reading & writing: Fill in the blanks',
                 'slug'=>'rwfb',
                 'category'=>'reading',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Summarize spoken text',
                 'slug'=>'sst',
                 'category'=>'listening',
                 'created_at' => now(),
                 'updated_at' => now(),

            ],
            [
                'q_type' => 'Fill in the blanks',
                 'slug'=>'finb',
                 'category'=>'listening',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Highlight correct summary',
                 'slug'=>'hcs',
                 'category'=>'listening',
                 'created_at' => now(),
                 'updated_at' => now(),
            ],
            [
                'q_type' => 'Select missing word',
                 'slug'=>'smw',
                 'category'=>'listening',
                 'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'q_type' => 'Highlight incorrect words',
                 'slug'=>'hiw',
                 'category'=>'listening',
                 'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'q_type' => 'Write from dictation',
                 'slug'=>'wfd',
                 'category'=>'listening',
                 'created_at' => now(),
                'updated_at' => now(),
            ],

        ];
        DB::table('question_types')->insert($inputFields2);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
       
       $permissions = [
            //user
            [
                'name' => 'view_user',
                'modules' => 'user',
                'display_name' => 'view user',
                'description' => 'view user',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'create_user',
                'modules' => 'user',
                'display_name' => 'create user',
                'description' => 'create user',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'edit_user',
                'modules' => 'user',
                'display_name' => 'edit user',
                'description' => 'edit user',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'delete_user',
                'modules' => 'user',
                'display_name' => 'delete user',
                'description' => 'delete user',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            //categories
            [
                'name' => 'view_category',
                'modules' => 'category',
                'display_name' => 'view category',
                'description' => 'view category',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'create_category',
                'modules' => 'category',
                'display_name' => 'create category',
                'description' => 'create category',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'edit_category',
                'modules' => 'category',
                'display_name' => 'edit category',
                'description' => 'edit category',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'delete_category',
                'modules' => 'category',
                'display_name' => 'delete category',
                'description' => 'delete category',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // question sets
            [
                'name' => 'view_question_sets',
                'modules' => 'question sets',
                'display_name' => 'view question sets',
                'description' => 'view question sets',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'create_question_sets',
                'modules' => 'question sets',
                'display_name' => 'create question sets',
                'description' => 'create question sets',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'edit_question_sets',
                'modules' => 'question sets',
                'display_name' => 'edit question sets',
                'description' => 'edit question sets',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'delete_question_sets',
                'modules' => 'question sets',
                'display_name' => 'delete question sets',
                'description' => 'delete question sets',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // question sets
            [
                'name' => 'view_roles_permissions',
                'modules' => 'roles',
                'display_name' => 'view roles permissions',
                'description' => 'view roles permissions',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'create_roles_permissions',
                'modules' => 'roles',
                'display_name' => 'create roles permissions',
                'description' => 'create roles permissions',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'edit_roles_permissions',
                'modules' => 'roles',
                'display_name' => 'edit roles permissions',
                'description' => 'edit roles permissions',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'delete_roles_permissions',
                'modules' => 'roles',
                'display_name' => 'delete roles permissions',
                'description' => 'delete roles permissions',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // question sets
            [
                'name' => 'view_settings',
                'modules' => 'settings',
                'display_name' => 'view settings',
                'description' => 'view settings',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'create_settings',
                'modules' => 'settings',
                'display_name' => 'create settings',
                'description' => 'create settings',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'edit_settings',
                'modules' => 'settings',
                'display_name' => 'edit settings',
                'description' => 'edit settings',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'delete_settings',
                'modules' => 'settings',
                'display_name' => 'delete settings',
                'description' => 'delete settings',
                'created_at' => now(),
                'updated_at' => now(),
            ],

        ];

        DB::table('permissions')->insert($permissions);
         */

        DB::table('answer_status')->insert([
            [
                'name' => 'read'
            ],
            [
                'name' => 'answered'
            ],
            [
                'name' => 'locked'
            ],
        ]);
    }
}
