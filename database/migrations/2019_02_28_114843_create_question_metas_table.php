<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_metas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('content_id');
            $table->foreign('content_id')->references('id')->on('questions')->onDelete('cascade');
            $table->string('meta_key',150);
            $table->longText("meta_value");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_metas');
    }
}
