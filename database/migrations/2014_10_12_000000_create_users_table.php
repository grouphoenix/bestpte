<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned()->index();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone',20)->index();
            $table->string('first_name',20)->nullable()->index();
            $table->string('last_name',20)->nullable()->index();
            $table->string('display_name');
            $table->date('dob')->nullable();
            $table->string('address', 60)->nullable();
            $table->enum('gender', array('male', 'female'))->default('male');
            $table->datetime('last_login')->nullable();
            $table->integer('login_count')->nullable();
            $table->integer('role_id')->unsigned()->index();
            $table->timestamp('email_verified_at')->nullable();
            $table->tinyInteger('status')->index();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
