<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::table('user_trackers', function (Blueprint $table) {
            $table->foreign('status')->references('id')->on('answer_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('user_trackers', function (Blueprint $table) {
            $table->dropForeign('user_trackers_status_foreign');
        });
        Schema::dropIfExists('answer_status');
    }
}
