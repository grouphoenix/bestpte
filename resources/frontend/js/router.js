import Vue from "vue";
import Router from "vue-router";
import Question from "./components/QuestionForm.vue";
Vue.use(Router);
export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [{
        path: "/pte/exam/set/:id/question/:question",
        name: "exam",
        component: Question
    }, ]
})