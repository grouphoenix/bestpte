import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        title: "BestPte Software Company",
    },
    getters: {
        getUserInfo(state) {
            return state.userInfo;
        },
    },
    actions: {
        async initStore({ commit }) {
            return axios.get('/userProfile').then(response => response.data)
                .then(userInfo => {
                    commit('SET_USER_INFO', userInfo)
                })
        }
    },
    mutations: {
        SET_USER_INFO(state, data) {
            state.userInfo = data;
        }
    }
});