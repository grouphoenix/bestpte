/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue');

import VueResource from 'vue-resource';

import router from './router';
import store from './store';
import jplayer from './lib/jplayer';
import AudioRecorder from "./index";
// tell Vue to use the vue-resource plugin
Vue.use(VueResource);
Vue.use(AudioRecorder);
Vue.config.productionTip = false;
Vue.component('ring-loader', require('../../js/components/RingLoader.vue').default);
Vue.component('forgot-password-component', require('./components/ForgetPasswordComponent.vue').default);
Vue.component('my-profile', require('../../js/components/profile.vue').default);
Vue.component('my-cart', require('./components/cart.vue').default);
Vue.component('checkout-app', require('./components/checkout.vue').default);
Vue.component('small-loader', require('./components/smallLoader.vue').default);
Vue.component('unlock-btn', require('./components/unlock.vue').default);
Vue.component('form-error', require('./components/FormError.vue').default);
Vue.component('general-page-component', require("./components/GeneralPage.vue").default);
Vue.component('question-component', require("./components/QuestionForm.vue").default);
window.Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

window.Laravel = {
    "baseUrl": "https:\/\/bestpte.test\/"
}

window.Swal = require('sweetalert2');

window.Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
});
if (document.getElementById('front-app')) {
    new Vue({
        router,
        store,
    }).$mount("#front-app");
}