@extends('bpc-admin.layout.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1> Category Listings</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                   <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item "><a href="{{route('category.lists')}}">Categories</a></li>
                    <li class="breadcrumb-item active">Update</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content" id="category">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-6">
                <form role="form" id="formCategory" @submit.prevent="handleSubmit()">
                  
                    <div class="card card-info">
                        <div class="card-header">
                        <h3 class="card-title">Update Category</h3>
                        </div>
                        <div class="card-body">
                            <div :class="errors.category_name ? ' form-group  has-errors' : 'form-group ' ">
                                <label for="category_name">Category Name</label>
                                <input type="text" v-model="category" name="category_name" value="{!! $detail->cat_name !!}" class="form-control" autoComplete="off" name="category_name"
                                    placeholder="Category Name" />
                                <small class="help-block" v-if=errors.category_name :errors=errors>@{{errors.category_name[0]}}</small>
                            </div>
                            <div class="form-group">
                                <label for="slug">Friendly Url</label>
                                <input type="text" name="slug" :value=slugify  disabled autoComplete="off" class="form-control"
                                    placeholder="Friendly Url" />
                                <code>{!! env('APP_URL')!!}/<span>@{{ slugify }}</span></code>
                            </div>
                            <div class="form-group">
                                <label for="parent_id">Parent</label>
                                <select name="parent_id" id="parent_id" v-model="parent_id" autoComplete="off" class="form-control">
                                <option value="0">Parent</option>
                                    {!!categorydropdownListing($categories,$detail->parent_id)!!}
                                   
                                </select>
                            </div>
                            @php
                                  
                                @endphp
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" v-model="status"  class="form-control">
                                    <option value="0">Active</option>
                                    <option value="1">Deactive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="modules_id">Attach Question Type</label>
                                <div class="row">
                                <div id="types" class="col-12">
                                <select name="modules_id[]" multiple class="form-control">
                                @foreach($types as $type)
                                @php
                                  $selected=(in_array_r($type->id,$detail->modules->toArray())) ? "selected" : "";
                                @endphp
                                <option value="{{$type->id}}" {{$selected}} >{{$type->q_type}}</option>
                                @endforeach
                                </select>
                                </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="hidden" name="district_id" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    </form>
                </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,

    });
    window.FireEvent=new Vue(); 
    const app = new Vue({
        el: '#category',
        data() {
            return {
                loading:true,
                id:"{!! $detail->id !!}",
                category: "{!! $detail->cat_name !!}",
                slug: "{!! $detail->slug !!}",
                parent_id: "{!! $detail->parent_id !!}",
                status: "{!! $detail->is_active !!}",
                submitted: false,
                errors: []
            }
        },
        computed: {
            slugify: function () {
                var slug = this.sanitizeTitle(this.category);
                return this.slug = slug;
            }
        },
        methods: {
            handleSubmit() {
                var form = document.getElementById('formCategory');
                var formData = new FormData(form);
                formData.append('slug', this.slug);
                formData.append('id', this.id);
                axios.post("/bpc-admin/category/updateCategory", formData).then((response) => {
                    Toast.fire({
                        type: 'success',
                        title: response.data.message
                    })
                }).catch((err) => {
                    this.errors = err.response.data.errors;
                });

            },
            sanitizeTitle(title) {
                var slug = "";
                // Change to lower case
                var titleLower = title.toLowerCase();
                // Letter "e"
                slug = titleLower.replace(/e|é|è|ẽ|ẻ|ẹ|ê|ế|ề|ễ|ể|ệ/gi, 'e');
                // Letter "a"
                slug = slug.replace(/a|á|à|ã|ả|ạ|ă|ắ|ằ|ẵ|ẳ|ặ|â|ấ|ầ|ẫ|ẩ|ậ/gi, 'a');
                // Letter "o"
                slug = slug.replace(/o|ó|ò|õ|ỏ|ọ|ô|ố|ồ|ỗ|ổ|ộ|ơ|ớ|ờ|ỡ|ở|ợ/gi, 'o');
                // Letter "u"
                slug = slug.replace(/u|ú|ù|ũ|ủ|ụ|ư|ứ|ừ|ữ|ử|ự/gi, 'u');
                // Letter "d"
                slug = slug.replace(/đ/gi, 'd');
                // Trim the last whitespace
                slug = slug.replace(/\s*$/g, '');
                // Change whitespace to "-"
                slug = slug.replace(/\s+/g, '-');
                return slug;
            }

        },

    });

</script>
@endpush
