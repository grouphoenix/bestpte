@extends('bpc-admin.layout.master')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5>Questions Type Section</h5>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.admin')}}">Home</a></li>
              <li class="breadcrumb-item active">Type Lists</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content" id="types">
      <div class="row">
          <div class="col-7 mr-auto ml-auto">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Types Lists</h3>
            </div>
            <div id="app">
            <question-field-type></question-field-type>
            </div>
        </div>
      </div>      
      
    </section>
@endsection
