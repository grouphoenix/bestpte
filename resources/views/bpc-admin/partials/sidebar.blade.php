<aside class="main-sidebar sidebar-dark-warning elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/dashboard')}}" target="_blank" class="brand-link bg-primary pb-2 d-flex justify-content-center">
        <img src="{{asset('admin/icon/logo.png')}}" alt="BPC Logo" class="brand-image brand-text">
    </a>
    <div class="clearfix"></div>

    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex" @if(auth()->user()) onclick="$('#userProfile').modal('show')" @endif>
            <div class="image">
                <img src="{{auth()->user()->image}}" width="50" height="50" class="img-circle" />
            </div>
            <div class="info">
                <a href="" onclick="prevent(event)" class="d-block">@if(auth()->user()){{strtoupper(auth()->user()->display_name)}} @else Administration @endif</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="/bpc-admin/" class="nav-link {{navActive('bpc-admin')}}">
                        <i class="fas fa-tachometer-alt nav-icon"></i>
                        <p>Dashboards</p>
                    </a>
                </li>
                @if(canNavigate('category'))
                    <li class="nav-item">
                        <a href="{{route('category.lists')}}" class="nav-link {{navActive('category','lists')}}">
                        <i class="fas fa-bars nav-icon"></i>
                            <p>Categories</p>
                        </a>
                    </li>

                @endif
                @if(canNavigate('user'))
                    <li class="nav-item has-treeview  {{currentActiveLink('user')}}">
                        
                        <a href="#" class="nav-link {{currentActiveLink('user')}}">
                            <i class="fas fa-user-circle nav-icon"></i>
                            <p>Users<i class="right fa fa-angle-left"></i></p>
                        </a>

                        <ul class="nav nav-treeview" {{currentViewBlock('user')}}>
                            @can('view_user')
                                <li class="nav-item">
                                    <a href="{{route('user.lists')}}" class="nav-link {{navActive('user','lists')}}" >
                                        <i class="nav-icon"></i>
                                        <p>All Users</p>
                                    </a>
                                </li>
                            @endcan
                            @can('create_user')
                                <li class="nav-item">
                                    <a href="{{route('user.create')}}" class="nav-link {{navActive('user','create')}}">
                                        <i class="nav-icon"></i>
                                        <p>Add New</p>
                                    </a>
                                </li>
                            @endcan
                            @if(canNavigate('roles'))
                                <li class="nav-item">
                                    <a href="/bpc-admin/role/list" class="nav-link">
                                    <i class="nav-icon"></i>
                                        <p>Roles & Permissions</p>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(canNavigate('question sets'))
                    <li class="nav-item has-treeview {{currentActiveLink('sets')}}">
                        <a href="#" class="nav-link {{currentActiveLink('sets')}}">
                            <i class="fab fa-acquisitions-incorporated"></i>
                            <p>Questions Sets<i class="right fa fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview" {{currentViewBlock('sets')}}>
                            @can('view_question_sets')
                                <li class="nav-item">
                                    <a href="{{route('set.lists')}}" class="nav-link {{navActive('sets','lists')}}">
                                        <i class="nav-icon"></i>
                                        <p>All Sets</p>
                                    </a>
                                </li>
                            @endcan
                            @can('create_question_sets')
                                <li class="nav-item">
                                    <a href="{{route('set.question.all')}}" class="nav-link {{navActive('questions')}}">
                                    <i class="nav-icon"></i>
                                        <p>Questions</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endif
                @if(canNavigate('settings'))
                    <li class="nav-item has-treeview {{currentActiveLink('settings')}}">
                        <a href="#" class="nav-link">
                            <i class="fas fa-sliders-h nav-icon"></i>
                            <p>Configuration<i class="right fa fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview" {{currentViewBlock('questions')}}>
                            @can('view_settings')
                                <li class="nav-item">
                                    <a href="/bpc-admin/settings" class="nav-link {{navActive('settings','/')}}">
                                        <i class="fas fa-cog nav-icon"></i>
                                        <p>Settings</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('type.lists')}}" class="nav-link {{navActive('types')}}">
                                        <i class="fas fa-books nav-icon"></i>
                                        <p>Types</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                   
                @endif
                <li class="nav-item">
                    <a href="/admincp/users" class="nav-link"  href="{{ route('auth.logout') }}" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <i class="fas fa-power-off nav-icon"></i>
                        <p> {{ __('Logout') }}</p>
                    </a>
                    <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
@push('scripts')
    <script>
        function prevent(e){
            e.preventDefault();
        }
    </script>
@endpush
