<!-- Modal -->
<div class="modal fade" id="userProfile" tabindex="-1" role="dialog" aria-labelledby="userProfile" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="app" >
            <div class="modal-header">
                <h3 class="modal-title ml-auto mr-auto">User Profile</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <my-profile></my-profile>
        </div>
    </div>
</div>
