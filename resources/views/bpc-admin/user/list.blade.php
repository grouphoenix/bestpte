@extends('bpc-admin.layout.master')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User Management Section</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/bpc-admin">Home</a></li>
              <li class="breadcrumb-item active">Users Listing</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Users Listing</h3>
              <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 250px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default btn-primary"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
                <a href="{{route('user.create')}}" class="addUser btn btn-default btn-primary ">Add new</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="user-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                   <th><input type="checkbox" name="selection" /></th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                  <th><input type="checkbox" class="minimal" name="checked" value="{{$user->id}}" /></th>
                  <td>{{$user->display_name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{ucwords($user->role_name)}}</td>
                  @if($user->status==0)
                  <td>Deactive</td>
                  @else
                  <td>Active</td>
                  @endif
                  <td class="center">
                    <a href="{{route('user.edit',$user->id)}}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i>Edit</a>
                    <a href="{{route('user.delete',$user->id)}}" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a></td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th><input type="checkbox" name="selection" /></th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>

              </table>
              <div class="clearfix"></div>
              <div class="pagination">
              {{ $users->links() }}
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          </div>
          </div>
    </section>


@endsection
@push('scripts')
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
    });
    window.FireEvent = new Vue();

    const app = new Vue({
        el: '#list-users'
    });
</script>
@endpush
