<!DOCTYPE html>
<html lang="{{config('app.locale')}}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.name').' | LOGIN'}}</title>
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/css/app.css')}}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>BEST</b> PTE CENTRE
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            @if (session('error'))
            <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
            @endif
            <p class="login-box-msg">Sign in to start your session</p>
            <form action="{{route('auth.login')}}" method="post" id="LoginForm">
                @csrf()
                <div class="form-group @if($errors->first('email')) has-errors @endif">
                    <div class="input-group mb-1 ">
                        <input type="text" name="email" autocomplete="off" class="form-control" placeholder="Email Id Provided">
                        <div class="input-group-append">
                            <span class="fa fa-envelope input-group-text"></span>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                    @if($errors->first('email'))
                        <small id="emailHelp" class="form-text">{{$errors->first('email')}}</small>
                    @endif
                </div>
                <div class="form-group @if($errors->first('email')) has-errors @endif">
                <div class="input-group mb-1">
                    <input type="password" name="password" autocomplete="off" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                        <span class="fa fa-lock input-group-text"></span>
                    </div>
                </div>
                    @if($errors->first('password'))
                        <small id="passHelp" class="form-text">{{$errors->first('password')}}</small>
                    @endif
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <!-- /.social-auth-links -->
            <p class="mb-1">
                <a href="#">I forgot my password</a>
            </p>

        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->
<!-- jQuery -->
<script src="{{asset('/admin/js/app.js')}}"></script>
</body>
</html>
