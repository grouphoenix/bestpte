@extends('bpc-admin.layout.master')
@section('content')
    <div class="settings">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Settings</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Settings</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="offset-md-1 col-md-10 ">
                        <div class="card card-default">
                            <div class="card-header">
                                <h4 class="card-title">Manage your web application setting</h4>
                            </div>
                            <form enctype="multipart/form-data" method="post" action="{{route('setting.update')}}">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="homepage" class="control-label">Homepage</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="default_homepage"
                                                       value="{{ $settings['default_homepage']  ?? '' }}" class="form-control" />
                                                <span class="help-block">Homepage of this website</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="site_title" class="control-label">Site Title</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" value="{!! $settings['site_title'] ?? '' !!}" name="site_title" class="form-control"/>
                                                <span class="help-block">Title of website.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="site_logo" class="control-label">Site logo</label>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="image-block">
                                                    <img id="site_logo" src="{!! $settings['site_logo'] ?? '' !!}" class="img-responsive" />
                                                    <a href=""><i class="fas fa-times"></i></a>
                                                </div>
                                                <div class="file-block">
                                                    <input type="file" accept="image/*" name="site_logo" id="logo"  onchange="previewImage(event)"/>
                                                    <a href="javascript:;" class="btn btn-primary" onClick="document.getElementById('logo').click();">Pick a logo </a>
                                                    <br />
                                                    <span class="help-block">Select logo for this site.</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="site_logo" class="control-label">Login Left Banner</label>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="image-block">
                                                    <img id="site_login_banner" src="{!! $settings['site_login_banner'] ?? '' !!}" class="img-responsive" />
                                                    <a href=""><i class="fas fa-times"></i></a>
                                                </div>
                                                <div class="file-block">
                                                    <input type="file" accept="image/*" name="site_login_banner" id="login_banner"  onchange="previewImage(event)"/>
                                                    <a href="javascript:;" class="btn btn-primary" onClick="document.getElementById('login_banner').click();">Pick a logo </a>
                                                    <br />
                                                    <span class="help-block">Select login left banner for this site.</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="site_logo" class="control-label">Register Left Banner</label>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="image-block">
                                                    <img id="site_register_banner" src="{!! $settings['site_register_banner'] ?? '' !!}" class="img-responsive" />
                                                    <a href=""><i class="fas fa-times"></i></a>
                                                </div>
                                                <div class="file-block">
                                                    <input type="file" accept="image/*" name="site_register_banner" id="register_banner"  onchange="previewImage(event)"/>
                                                    <a href="javascript:;" class="btn btn-primary" onClick="document.getElementById('register_banner').click();">Pick a logo </a>
                                                    <br />
                                                    <span class="help-block">Select Register left banner for this site.</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="site_keywords" class="control-label">Site keywords</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="site_keywords" class="form-control"  value="{!! $settings['site_keywords'] ?? '' !!}" />
                                                <span class="help-block">Use for SEO.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="site_description" class="control-label">Site description</label>
                                            </div>
                                            <div class="col-md-7">
                                                <textarea  name="site_description" class="form-control">{!! $settings['site_description'] ?? '' !!}</textarea>
                                                <span class="help-block">Use for SEO.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="Location" class="control-label">Location</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="location" value="{!! $settings['location']  ?? '' !!}" class="form-control"/>
                                                <span class="help-block">Official Location.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="official_phone_number" class="control-label">Official Phone
                                                    Number</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="official_phone_number" value="{!! $settings['official_phone_number'] ?? '' !!}" class="form-control" />
                                                <span class="help-block">Official Phone No.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="info_email" class="control-label">Email receives feedback
                                                    from clients</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="info_email" value="{!! $settings['info_email'] ?? '' !!}" class="form-control"/>
                                                <span class="help-block">Email receives feedback from clients</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="analytics" class="control-label">Google Analytics code</label>
                                            </div>
                                            <div class="col-md-7">
                                                <textarea  name="analytics"  class="form-control">{!!$settings["analytics"] ?? '' !!}</textarea>
                                                <span class="help-block">Add google analytics for site</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="google_captcha" class="control-label">Google captcha</label>
                                            </div>
                                            <div class="col-md-7">
                                                <label for="site_key" class="control-label">Site key</label>
                                                <input  type="text" name="site_key" value="{!!$settings['site_key'] ?? ''!!}" class="form-control"/>
                                                <label for="secret_key" class="control-label">Secret Key</label>
                                                <input type="text" name="secret_key" value="{!!$settings['secret_key'] ?? '' !!}" class="form-control"/>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="google_captcha" class="control-label">Paypal Credentials</label>
                                            </div>
                                            <div class="col-md-7">
                                                <label for="paypal_client_id" class="control-label">Paypal Client ID</label>
                                                <input  type="text" name="paypal_client_id" value="{!!$settings['paypal_client_id'] ?? ''!!}" class="form-control"/>
                                                <label for="paypal_secret" class="control-label">Paypal Secret</label>
                                                <div class="row">
                                                    <div class="col-10 pr-0">
                                                        <input type="password" name="paypal_secret" id="secretField" value="{!!$settings['paypal_secret'] ?? '' !!}" class="form-control" autocomplete="off"/>
                                                    </div> 
                                                    <div class="col-2 px-2"> 
                                                        <button class="btn btn-outline-primary" type="button"  id="secretBtn">Show</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="facebook_link" class="control-label">Facebook</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="facebook_link" value="{!!$settings['facebook_link'] ?? '' !!}" class="form-control"/>
                                                <span class="help-block">Facebook page.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="twitter_link" class="control-label">Twitter</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="twitter_link" value="{!!$settings['twitter_link'] ?? '' !!}" class="form-control"/>
                                                <span class="help-block">twitter page.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="youtube_link" class="control-label">Youtube</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input  type="text" name="youtube_link" value="{!! $settings['youtube_link'] ?? '' !!}" class="form-control"/>
                                                <span class="help-block">youtube page.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="general_set_intro" class="control-label">General Set of Instructions</label>
                                            </div>
                                            <div class="col-md-7">
                                                <textarea name="general_set_intro" class="form-control"></textarea>
                                                <span class="help-block">General Set of Instructions</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="general_set_intro" class="control-label">Global Time Frame (sec)</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input name="global_time_frame" value="{!! $settings['global_time_frame'] ?? '' !!}" class="form-control"/>
                                                <span class="help-block">General Set of Instructions</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="general_set_intro" class="control-label">Global Start Time (sec)</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input name="global_start_time" value="{!! $settings['global_start_time'] ?? '' !!}" class="form-control"/>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="general_set_intro" class="control-label">General Set of Instructions</label>
                                            </div>
                                            <div class="col-md-7">
                                                <textarea name="general_set_intro" class="form-control"/></textarea>
                                                <span class="help-block">General Set of Instructions</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="construction_mode" class="control-label">Construction Mode</label>
                                            </div>
                                            <div class="col-md-7">
                                            @if (isset($settings['construction_mode']) && $settings['construction_mode'] == 1)
                                                <input value="1" type="checkbox"  name="construction_mode" checked class="flat-red" />In construction mode
                                                <br />
                                            @else
                                            <input value="1" type="checkbox"  name="construction_mode" class="flat-red" />In construction mode
                                                <br />
                                            @endif
                                                <span class="help-block">Make the site is on construction mode.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="update" class="btn btn-danger btn-lg">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        let previewImage=(e)=>{
             let id= e.target.getAttribute('id');
            const output = document.getElementById("site_"+id);
            output.src = URL.createObjectURL(event.target.files[0]);
        }

        let secretBtn = document.getElementById('secretBtn');
        secretBtn.addEventListener('click', togglePassword);

        function togglePassword(e){
            e.preventDefault();
            e.target.classList.toggle('btn-outline-primary');
            e.target.classList.toggle('btn-outline-danger');
            let secretField = document.getElementById('secretField');
            if(secretField.type==='password'){
                secretField.type = 'text';
                e.target.innerHTML ='Hide';
            } else{
                secretField.type = 'password';
                e.target.innerHTML = 'Show';
            }
        }
    </script>
@endpush
@push('styles')
<style>
        .help-block {
            font-size: 12px
        }

        #logo,#login_banner,#register_banner {
            display: none;
        }

        .image-block {
            position: relative;
        }

        .fas.fa-times {
            position: absolute;
            background: #d8d2d2;
            padding: 6px;
            border-radius: 50%;
            height: 25px;
            width: 25px;
            text-align: center;
            color: #000;
            left: 85px;
            top: -10px;
        }

        #site_logo,#site_login_banner,#site_register_banner {
            width: 21%;
            height: auto;
        }

        textarea {
            margin-top: 0px;
            margin-bottom: 0px;
            height: 120px !important;
        }
    </style>
@endpush
