<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.name','Best PTE Centre')}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('admin/css/app.css')}}">
    @stack('styles')

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id='app'>
    @include('bpc-admin.partials.navbar')
    @include('bpc-admin.partials.sidebar')
    <div class="content-wrapper">
       @yield('content')
    </div>
    @include('bpc-admin.partials.userProfile')
    </div>
<!-- ./wrapper -->
<script type="text/javascript" src="{{ asset('admin/js/app.js') }}"></script>
@stack('scripts')
</body>
</html>
