<?php include('header.php')?>
    <div class="wrapper">
        <?php include('navbar.php')?>
        <aside class="main-sidebar  d-flex">
            <div class="sidebar m-t-25">
                <div class="list-group d-lg-block d-md-block d-sm-none .d-xs-none">
                    <a href="dashboard.php" class="list-group-item  list-group-item-action flex-column align-items-start">
                        <img src="images/test-icon.png" class="img-fluid">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 mt-2">Section wise test</h5>
                        </div>
                    </a>
                    <a href="fullmockup.php" class="list-group-item active m-t-10 list-group-item-action flex-column align-items-start">
                        <img src="images/test-icon-active.png" class="img-fluid">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 mt-2">Full Mock Test</h5>
                        </div>
                    </a>
                    <a href="scored-mockup.php" class="list-group-item  m-t-10 list-group-item-action flex-column align-items-start">
                        <img src="images/test-icon.png" class="img-fluid">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 mt-2">Scored Mock Test</h5>
                        </div>
                    </a>
                </div>
                <ul class="list-group-horizontal d-block d-sm-block d-xs-block d-lg-none d-md-none ">
                    <li class="list-group-item">
                        <a href="scored-mockup.php" class="list-group-item activ m-t-10 list-group-item-action flex-column align-items-start">
                            <img src="images/test-icon.png" class="img-fluid">
                            <div class="w-100 justify-content-between">
                                <h5 class="mb-1 mt-2">Section wise test</h5>
                            </div>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="scored-mockup.php" class="list-group-item  m-t-10 list-group-item-action flex-column align-items-start">
                            <img src="images/test-icon.png" class="img-fluid">
                            <div class="w-100 justify-content-between">
                                <h5 class="mb-1 mt-2">Full Mock Test</h5>
                            </div>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="scored-mockup.php" class="list-group-item  m-t-10 list-group-item-action flex-column align-items-start">
                            <img src="images/test-icon.png" class="img-fluid">
                            <div class="w-100 justify-content-between">
                                <h5 class="mb-1 mt-2">Scored Mock Test</h5>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </aside>
        <div class="content-wrapper m-t-25">
            <section class="dashboard">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-speaking" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div class="scoredmock">
                                        <div class="row">
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                            <h4>Scored Mocked Test 1</h4>
                                                    </div>
                                                          <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                   </div>
                                                    <div class="score-mock-link ">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                               </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                <div class="scored-test d-flex justify-content-center">
                                                    <h4>Scored Mocked Test 2</h4>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="duration d-flex ">
                                                    <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                </div>
                                                <div class="score-mock-link">
                                                    <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 3</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <div class="score-mock-link ">
                                                            <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                        </div>
                                                </div>

                                               </div>

                                                </div>
                                            </div>
                                        <div class="row">
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 4</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 5</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 6</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <div class="score-mock-link ">
                                                            <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 7</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 8</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 9</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <div class="score-mock-link ">
                                                            <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 10</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 11</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 12</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <div class="score-mock-link ">
                                                            <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 13</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 14</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link">
                                                        <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="scoredmock">
                                                    <div class="scored-test d-flex justify-content-center">
                                                        <h4>Scored Mocked Test 15</h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="duration d-flex ">
                                                        <span>Duration:1hr 15min <i class="fas fa-stopwatch"></i></span>
                                                    </div>
                                                    <div class="score-mock-link ">
                                                        <div class="score-mock-link ">
                                                            <a href="" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
        <div class="clearfix"></div>

    </div>
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-center">
                        <p><span>Best Pte Centre &copy; Copyright 2019 </span>. All Right Reserved</p>
                    </div>

                </div>
            </div>
        </div>
    </footer>
<?php include("footer.php");?>