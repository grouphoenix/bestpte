<aside class="main-sidebar  d-flex">
            <div class="sidebar m-t-25">
                <div class="list-group d-lg-block d-md-block d-sm-none .d-xs-none">
                    <a href="dashboard.php" class="list-group-item  list-group-item-action flex-column align-items-start">
                        <img src="{{asset('images/test-icon.png')}}'}}" class="img-fluid">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 mt-2">Section wise test</h5>
                        </div>
                    </a>
                    <a href="fullmockup.php" class="list-group-item active m-t-10 list-group-item-action flex-column align-items-start">
                        <img src="{{asset('images/test-icon-active.png')}}" class="img-fluid">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 mt-2">Full Mock Test</h5>
                        </div>
                    </a>
                    <a href="scored-mockup.php" class="list-group-item  m-t-10 list-group-item-action flex-column align-items-start">
                        <img src="{{asset('images/test-icon.png')}}" class="img-fluid">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 mt-2">Scored Mock Test</h5>
                        </div>
                    </a>
                </div>
                    <ul class="list-group-horizontal d-block d-sm-block d-xs-block d-lg-none d-md-none ">
                        <li class="list-group-item">
                            <a href="scored-mockup.php" class="list-group-item activ m-t-10 list-group-item-action flex-column align-items-start">
                            <img src="{{asset('images/test-icon.png')}}" class="img-fluid">
                            <div class="w-100 justify-content-between">
                                <h5 class="mb-1 mt-2">Section wise test</h5>
                            </div>
                            </a>
                          </li>
                        <li class="list-group-item">
                            <a href="scored-mockup.php" class="list-group-item  m-t-10 list-group-item-action flex-column align-items-start">
                            <img src="{{asset('images/test-icon.png')}}" class="img-fluid">
                            <div class="w-100 justify-content-between">
                                <h5 class="mb-1 mt-2">Full Mock Test</h5>
                            </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="scored-mockup.php" class="list-group-item  m-t-10 list-group-item-action flex-column align-items-start">
                            <img src="{{asset('images/test-icon.png')}}" class="img-fluid">
                            <div class="w-100 justify-content-between">
                                <h5 class="mb-1 mt-2">Scored Mock Test</h5>
                            </div>
                            </a>
                        </li>
                    </ul>
            </div>
        </aside>