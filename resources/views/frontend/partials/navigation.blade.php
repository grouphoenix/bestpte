<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand">
        <img src="{{$CMSSettings['site_logo'] ?? asset('images/logo-100.jpg')}}" class="img-fluid">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav ml-auto mr-auto">
         <h5 class="dash-h1">{{$CMSSettings['default_homepage'] ?? 'best pte center dashboard'}}</h5>
        </div>
        <div class="navbar-nav my-1 my-lg-0 user-cart">
            <my-cart />
        </div>
        <div class=" my-1 my-lg-0 user-image" style="background:url('{{auth()->user()->image}}') no-repeat;">
        </div>
        <div class="navbar-nav my-1 my-lg-0 user-dropdown">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  @auth
                   {{auth()->user()->display_name}}
                   @endauth
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#userProfile">Profile</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('logout')}}">Logout</a>

                </div>
            </li>
        </div>
    </div>
</nav>

@push('scripts')
    <script>
        if(window.menubar.visible){
            document.querySelector('.user-image').style.display= 'block';
            document.querySelector('.user-dropdown').style.display= 'block';
            document.querySelector('.user-cart').style.display= 'block';
            document.querySelector('.navbar-brand').href= '/dashboard';
        }
    </script>
@endpush

@push('styles')
<style>
.dropdown-menu{
    padding: 0 0 0.5rem 0;
}
.user-image, .user-dropdown, .user-cart{
    display:none;
}
</style>
@endpush
