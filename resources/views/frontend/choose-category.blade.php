@extends('frontend.layout.master')
@section('content')
@section('navbar')
 @include('frontend.partials.navigation')
 @endsection
<section class="choose-category">
 <div class="container">
     <div class="row">
         <div class="col-12">
             <div class="heading">
                 <h2 class="d-flex justify-content-center">Choose one category</h2>
                 <img src="images/seperator-category.png"  alt="category seprator" class="img-fluid mr-auto ml-auto seperator">
             </div>
         </div>
         <div class="col-12">
             <div class="row">
                 <div class="cat-boxes d-lg-flex">
                     <div class="col-4 offset-2 d-lg-flex align-items-lg-start">
                         <div class="cat-box">
                             <a href="{{url('dashboard/test/'.$initcat->cat_slug)}}">PTE</a>
                         </div>
                     </div>
                     <div class="col-4  d-lg-flex align-items-lg-end">
                         <div class="cat-box">
                             <a href="{{url('dashboard')}}">ILETS</a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
</section>
 @section('footer-script')
 @include('frontend.partials.footer-script')
 @endsection
@endsection
