@extends('frontend.layout.master')
@section('content')
@section('navbar')
@include('frontend.partials.navigation')
@endsection
<question-component v-bind:qset="{{ $data['set_id'] }}" :page="{{$data['page']}}"></question-component>
@section('footer-script')
@include('frontend.partials.footer-script')
@endsection
@endsection