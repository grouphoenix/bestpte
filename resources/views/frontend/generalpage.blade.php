@extends('frontend.layout.master')
@section('content')
@section('navbar')
@include('frontend.partials.navigation')
@endsection
<general-page-component :id="{{$data}}"></general-page-component>
@section('footer-script')
@include('frontend.partials.footer-script')
@endsection
@endsection