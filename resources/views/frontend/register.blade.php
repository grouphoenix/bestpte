@extends('frontend.layout.master')
@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')
<section class="register-section">
	<div class="container-fluid">
		<div class="row center-row">
			<div class="col-lg-7 p-0 mbl-hide">
				<div class="register-left-img" style="background-image: url({{$CMSSettings['site_register_banner'] ?? 'images/register-banner.jpg'}});">  
					<img src="{{$CMSSettings['site_logo'] ?? 'images/logo.png'}}" alt="" class="img-responsive"> 
				</div> 
			</div>
			<div class="col-sm-12 mbl-show-logo">
				<img src="images/logo.png" alt="">
			</div>
			<div class="col-lg-5 p-0">
				<div class="register-right-form">  
					<div class="register-title"> 
						<h1>Regsiter</h1> 
						<img src="images/login-seprator.png" alt="register seprator">
					</div> 
					<div class="mx-auto">
						<form class="register-form" action="{{ route('register') }}" method="POST">
                          @csrf
							<div class="form-group @if ($errors->has('display_name')) has-errors @endif"> 
								<input type="text" value="{{ old('display_name') }}" name="display_name" class="form-control" placeholder="Full Name"> 
								@if ($errors->has('display_name'))
								<small class="form-text">{{$errors->first('display_name')}}</small>
                                @endif
							</div>
							<div class="form-group  @if ($errors->has('email')) has-errors @endif"> 
								<input type="email" value="{{ old('email') }}"  name="email" class="form-control" placeholder="Email"> 
								@if ($errors->has('email'))
								<small class="form-text">{{$errors->first('email')}}</small>
                                @endif
							</div>
							<div class="form-group @if ($errors->has('password')) has-errors @endif"> 
								<input type="password"  value="{{ old('password') }}" name="password" class="form-control"  placeholder="Password"> 
								@if ($errors->has('password'))
								<small class="form-text">{{$errors->first('password')}}</small>
                                @endif
                               </div>
							<div class="form-group  @if ($errors->has('phone')) has-errors @endif"> 
								<input type="text" value="{{ old('phone') }}" name="phone" class="form-control" placeholder="phone">
								@if ($errors->has('phone'))
								<small class="form-text">{{$errors->first('phone')}}</small>
                                @endif 
							</div>
							<div class="form-group @if ($errors->has('address')) has-errors @endif"> 
								<input type="text" value="{{ old('address') }}" name="address" class="form-control" placeholder="Address">
								@if ($errors->has('address'))
								<small class="form-text">{{$errors->first('address')}}</small>
                                @endif  
							</div>
							<div class="form-group @if ($errors->has('country')) has-errors @endif">
								<select class="custom-select country" name="country" value="{{ old('country') }}">
						            <option value="" selected disabled>Country...</option>
						            @foreach($countries as $country)
									<option value="{{$country->id}}">{{$country->country_name}}</option>
									@endforeach
					            </select>
								@if ($errors->has('country'))
								<small id="country" class="form-text">{{$errors->first('country')}}</small>
                                @endif  
					        </div> 
							<div class="form-group"> 
								<label>{{ __('Have you sit for PTE Academic before?') }}</label>
							   <select class="custom-select" name="pte_academic" id="academic" value="{{ old('pte_academic') }}">
							        <option value="no">No</option>
						            <option value="yes">Yes</option>
					              </select>
							</div>
							<div class="form-group pte-score hide"> 
								<input type="text" value="{{ old('pte_score') }}" name="pte_score" class="form-control" placeholder="Previous Score in PTE Academic."> 
							</div>
							<button class="btn-button btn-register" type="submit">Register</button>
						</form> 
						<div class="or-line">
							<hr class="hr-or-1">
							<span>OR</span>
							<hr class="hr-or-2">
						</div> 
						<div class="gmail-facebook-sign"> 
						<a  href="{{route('social_login',['google'])}}" class="btn-gmail" > 
								<img src="images/google-img.png" alt="">
								Sign in with Gmail
							</a>
							<a href="#" class="btn-facebook">
							<i class="fab fa-facebook-square"></i>
								Sign in with Facebook
							</a> 
						</div> 
						<p class="signup-account-p">Already have an account?<a href="{{url('/')}}" class="">Sign In</a></p>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
$(function(){
	$("#academic").on("change",function(e){
		var value=e.target.value;
		if(value==="yes")
		{
			$(".pte-score").removeClass('hide');
		}else{
			$(".pte-score").addClass('hide');
		}
	})
})
</script>

@endpush