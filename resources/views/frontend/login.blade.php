@extends('frontend.layout.master')
@section('content')
<section class="login-section">
	<div class="container-fluid">
		<div class="row center-row">
			<div class="col-lg-7 p-0 mbl-hide">
				<div class="login-left-img" style="background-image: url({{$CMSSettings['site_login_banner'] ?? 'images/login-banner.jpg'}});">    
					<img src="{{$CMSSettings['site_logo'] ?? 'images/logo.png'}}" alt="Best pte centre" class="img-responsive"> 
				</div>
			</div>
			<div class="col-sm-12 mbl-show-logo">
				<img src="images/logo.png" alt="">
			</div>
			<div class="col-lg-5 p-0">
				<div class="login-right-form">  
					<div class="login-title">  
						<h1>LOGIN</h1> 
						<img src="images/login-seprator.png" alt="login seprator">
					</div> 
					<div class="mx-auto">
						<form class="login-form" action="{{route('login')}}" method="post">
						@csrf()
							<div class="form-group @if($errors->first('email')) has-errors @endif">
								<label for="login-email">Email</label>
								<input type="text" id="login-email" name="email" autocomplete="off" class="form-control">
								@if($errors->first('email'))
								<small id="emailHelp" class="form-text">{{$errors->first('email')}}</small>
								@endif
							</div>
							<div class="form-group @if($errors->first('password')) has-errors @endif">
								<label for="login-password">Password</label>
								<input type="password" id="login-password" name="password" autocomplete="off" class="form-control">
								@if($errors->first('password'))
                        <small id="passHelp" class="form-text">{{$errors->first('password')}}</small>
                         @endif
							</div> 
							<button class="btn-button btn-login btn-hover-gradient " type="submit">LOGIN</button>
						</form>
						<div id="forgot-app">
						<forgot-password-component></forgot-password-component>
						</div>
						<div class="or-line">
							<hr class="hr-or-1">
							<span>OR</span>
							<hr class="hr-or-2">
						</div>
						<div class="gmail-facebook-sign"> 
							<a  href="{{route('social_login',['google'])}}" class="btn-gmail" > 
								<img src="images/google-img.png" alt="">
								Sign in with Gmail
							</a>
							<a href="#" class="btn-facebook">
							<i class="fab fa-facebook-square"></i>
								Sign in with Facebook
							</a> 
						</div>
						<p class="signup-account-p">Don't have an account?<a href="{{route('register')}}" class="">Sign Up</a></p>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
const app = new Vue({
    el: '#forgot-app'
});
</script>
@endpush