@extends('frontend.layout.master')

@section('content')
@section('navbar')
@include('frontend.partials.navigation')
@endsection
<div id="front-end">
<div class="container">
        <div class="row">
            <vue-loader :loading="loading"></vue-loader>
            <div :class="{'hidden': loading}">
                @foreach($data as $question)
                <div class="col-12">
                    <h3>{{$question->question_set_name}} : {{$question->cat_name}} - {{$question->q_type}}
                        <span class="btn btn-primary pull-right"><span id="remain-time"></span>/ <span id="total-time">00:30:00</span></span> &nbsp;&nbsp;
                        <span class="pull-right btn btn-warning">1 of 35</span></h3>
                    <p style='font-size:16px'>{{$question->additional_note}}</p>
                </div>
                <div class="recorderbox">
                </div>
                <div>
                    <p>{{$question->question}}</p>
                </div>
               @endforeach
            
               @if($nextpage!="")
              
               <a href="{{route('exam.question.detail',['id'=>$set_id,'page'=>$nextpage])}}">next</a>
               @else
               <button>Submit</button>

               @endif
            </div>

        </div>
    </div>
</div>
@section('footer-script')
@include('frontend.partials.footer-script')
@endsection
@endsection