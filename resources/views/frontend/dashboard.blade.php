@extends('frontend.layout.master')
@section('content')
@section('navbar')
@include('frontend.partials.navigation')
@endsection
<div class="main">
<aside class="main-sidebar  d-flex">
    <div class="sidebar m-t-25">
        <div class="list-group d-lg-block d-md-block d-sm-none d-xs-none">
            @php $i=0; @endphp
            @foreach($parentCat as $cat) @php $i++; @endphp
            <a href="{{route('cat.questionset',$cat->cat_slug)}}" class="list-group-item  {!! ($i==1) ? 'active' :'' !!} list-group-item-action flex-column align-items-start">
                <img src="{!! ($i==1) ? asset('images/test-icon-active.png') :asset('images/test-icon.png')!!}" class="img-fluid">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1 mt-2">{{$cat->cat_name}}</h5>
                </div>
            </a>
            @endforeach

        </div>
    </div>
</aside>
<div class="content-wrapper m-t-25">
    <section class="dashboard">
        <div class="container">
            <div class="row">
                <div class="col-12">
                @if($subcategories->children)
                    @php $i=0; @endphp
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        @foreach($subcategories->children as $sub) @php $i++; @endphp
                        <li class="nav-item">
                            <a class="nav-link {!! ($i==1) ? 'active' :''!!}" id="pills-{{$sub->cat_slug}}-tab"
                                data-toggle="pill" href="#pills-{{$sub->cat_slug}}" role="tab" aria-controls="pills-{{$sub->cat_slug}}"
                                aria-selected="true">{{ucwords($sub->cat_name)}}</a>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                    @if($subcategories->children)
                    @php $i=0; @endphp
                    <div class="tab-content" id="pills-tabsContent">
                        @foreach($subcategories->children as $sub)
                        @php $i++; @endphp
                        <div class="tab-pane fade show {{($i==1) ? 'active' :''}}" id="pills-{{$sub->cat_slug}}" role="tabpanel"
                            aria-labelledby="pills-{{$sub->cat_slug}}-tab">
                            <div class="{{$sub->cat_slug}}">
                                @foreach ($sub->question_sets->chunk(3) as $chunk)
                                <div class="row">
                                    @foreach ($chunk as $set)
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <div class="speaking-test">
                                            <h4>{{$set->question_set_name}}</h4>
                                            @if(count($set->questions)>0)
                                                <span class="count">{{count($set->questions)}} {{(count($set->questions)>1) ? "Questions" :" Question"}}</span>
                                                <div class="clearfix"></div>
                                            @else
                                            <span class="count">No Questions</span>
                                            <div class="clearfix"></div>
                                            @endif
                                            <span>Duration:{{convertToTime($set->question_set_time)}} <img src="{{asset('images/time_icon.png')}}" width="35"></span>
                                            <div class="clearfix"></div>
                                            @if($set->question_set_type!="free")
                                                @if( in_array($set->id,$cart))
                                                <div><unlock-btn :id={{$set->id}} display='none' /></div>
                                                    <a href="#" class="btn btn-default btn-checkout disabled" id='btn-checkout{{$set->id}}'><i class="fas fa-cart-plus"></i>In Cart</a>
                                                @elseif(in_array($set->id,$module) && !auth()->user()->state($set->id))
                                                <a href="{{route('exam.detail',$set->id)}}" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                @else
                                                    @if(auth()->user()->state($set->id))
                                                        @if(auth()->user()->state($set->id)->status==3) {{-- 3 == locked --}}
                                                            
                                                            <div><unlock-btn :id={{$set->id}} display='block' /></div>
                                                            <a href='#' class="btn btn-default btn-checkout disabled" id='btn-checkout{{$set->id}}'style='' ><i class="fas fa-cart-plus"></i>In Cart</a>
                                                        @else 
                                                        <a href="javascript:;" onClick="window.open('{{route('exam.detail',$set->id)}}','', 'toolbar=yes,scrollbars=yes,resizable=yes,width=1280,height=760')" class="btn btn-default btn-unlock" id='btn-unlock{{$set->id}}' ><i class="fas fa-play-circle"></i>Resume</a>
                                                        @endif
                                                    @else
                                                       
                                                        <div><unlock-btn :id={{$set->id}} display='block' /></div>
                                                        <a href='#' class="btn btn-default btn-checkout disabled" id='btn-checkout{{$set->id}}'style='display:none;' ><i class="fas fa-cart-plus"></i>In Cart</a>
                                                    @endif
                                                @endif
                                            @else
                                                @if(!auth()->user()->state($set->id))
                                                    <a href="javascript:;" onClick="window.open('{{route('exam.detail',$set->id)}}','', 'toolbar=yes,scrollbars=yes,resizable=yes,width=1280,height=760')" class="btn btn-default btn-start"><i class="fas fa-play-circle"></i> Start</a>
                                                @elseif(auth()->user()->state($set->id)->status==3)
                                                <a href="javascript:;" onClick="window.open('{{route('exam.detail',$set->id)}}','', 'toolbar=yes,scrollbars=yes,resizable=yes,width=1280,height=760')" class="btn btn-default btn-start disabled"><i class="fas fa-play-circle"></i> Start</a>
                                                @else
                                                <a href="javascript:;" onClick="window.open('{{route('exam.detail',$set->id)}}','', 'toolbar=yes,scrollbars=yes,resizable=yes,width=1280,height=760')" class="btn btn-default btn-unlock" id='btn-unlock{{$set->id}}' ><i class="fas fa-play-circle"></i>Resume</a>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                @endforeach
                            </div>
                        </div>

                        @endforeach
                    </div>
                    @endif

               </div>
            </div>
    </section>
</div>
</div>
@section('footer-script')
@include('frontend.partials.footer-script')
@endsection
@endsection
