@extends('frontend.layout.master')
@section('content')
    {{-- @if(session('success'))
        <div class="alert alert-success">
            {{session('success_message')}}
        </div>
    @endif
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}
    <div class="container">
        <div class="row mt-5 card">
            <div class="card-header text-center"><h4>Select a Payment Method</h4></div>
            <div class="col-8 offset-2 card-body">
                <div class="content"> 
                        <form method="post" id="payment-form" action="{{route('execute_payment')}}">
                            @csrf
                            <section>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total = 0;
                                        @endphp
                                        @foreach($cart as $item)
                                        <tr>
                                            <td>{{$item->question_set_name}}</td>
                                            <td>$ {{$item->question_set_price}}</td>
                                        @php
                                           $total += $item->question_set_price;
                                        @endphp
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td class="text-right"><b class="text-uppercase"><u>Total</u><b></td>
                                            <td>$ {{$total}}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="bt-drop-in-wrapper">
                                    <div id="bt-dropin"></div>
                                </div>
                            </section>

                            <input id="nonce" name="payment_method_nonce" type="hidden" />
                            <div class="d-flex flexed mt-5">
                                <div>
                                    <button class="button btn btn-primary" type="submit"><span>Make Payment</span></button>
                                </div>
                                <div></div>
                                <div>
                                    <a href="{{url('/dashboard')}}" class="btn btn-danger"><span>Cancel</span></a>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://js.braintreegateway.com/web/dropin/1.18.0/js/dropin.min.js"></script>
    <script>
        var form = document.querySelector('#payment-form');
        var client_token = "{{$token}}";
        braintree.dropin.create({
        authorization: client_token, 
        selector: '#bt-dropin',
        paypal: {
            flow: 'vault'
        }
        }, function (createErr, instance) {
        if (createErr) {
            console.log('Create Error', createErr);
            return;
        }
        form.addEventListener('submit', function (event) {
            event.preventDefault();
            instance.requestPaymentMethod(function (err, payload) {
            if (err) {
                console.log('Request Payment Method Error', err);
                return;
            }
            // Add the nonce to the form and submit
            document.querySelector('#nonce').value = payload.nonce;
            form.submit();
            });
        });
        });
    </script>
@endpush
@push('styles')
<style>
    .flexed div{
        flex:1;
        text-align: center;
    }
</style>
@endpush