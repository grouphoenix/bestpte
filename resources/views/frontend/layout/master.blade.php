<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}">
	<title>{{config('app.name',' BEST PTE CENTER')}}</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="{{asset("dist/skin/css/jplayer.blue.monday.css")}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    @stack('styles')
</head>
<body class="bg-main">
    <div class="wrapper" id="front-app" >
        @yield('navbar')
        @yield('sidebar')
        @yield('content')
        @yield('footer-script')
        <!-- Modal -->
        @auth
            @include('frontend.partials.userProfile')
            <div class="modal fade" id="checkout" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <checkout-app></checkout-app>
                    </div>
                </div>
            </div>
        @endauth
    </div>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@stack('scripts')
<script>
    $(function () { $('[data-toggle="tooltip"]').tooltip() })
</script>
</body>
</html>
