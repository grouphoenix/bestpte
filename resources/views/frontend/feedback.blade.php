@extends('frontend.layout.master')

@section('content')
@section('navbar')
@endsection
<div id="front-end">
  <div class="container-fluid bg-primary text-white text-center">
    <p>Welcome to BESTPTE center.</p>
  </div>
  <div class="container">
      <div class="row pt-4">
        <div class="col">
        <div class="alert alert-success text-center" role="alert">
          <h4>Congratilations! You have sucessfully completed your Exam. <br>
          You will get your scorecardin 24 hrs.</h4>
        </div>
        </div>
      </div>
      <div class="card border-primary">
        <div class="card-body">
          <div class="text-center">
            <h4 class="card-title">Rate your test.</h4>            
          </div>
          <form action="{{route('exam.feedback')}}" method="POST">
              @csrf
              <div class="row">
                  <div class="col-6">
                    <div class="card">
                      <div class="card-body">
                        <div class="text-center">
                          <h4 class="card-title">Quality of test.</h4>
                        </div>
                        <hr>
                          <div class="form-check form-check-inline">
                            <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="quality" value="poor"><span class="label">Poor</span>
                            </label>

                            <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="quality" value="average"><span class="label">Average</span>                        
                            </label>

                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="quality" value="good"><span class="label">Good</span>
                            </label>
                            
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="quality" value="excellent"><span class="label">Excellent</span>
                            </label>
                              
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="quality" value="outstanding"><span class="label">Outstanding</span>
                              </label>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-6">
                      <div class="card">
                          <div class="card-body">
                            <div class="text-center">
                              <h4 class="card-title">Difficulty level of test.</h4>
                            </div>
                            <hr>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="difficulty" value="poor"> So Easy
                                </label>
    
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="difficulty"  value="average"> Easy                        
                                </label>
    
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="difficulty"  value="good"> Average
                                </label>
                                
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="difficulty"  value="excellent"> Difficult
                                </label>
                                  
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="difficulty" value="outstanding"> So Difficult
                                  </label>
                              </div>
                          </div>
                        </div>
                  </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="form-group">
                  <label for="experience" style="color:black;">Please share your test experience</label>
                  <textarea class="form-control" name="experience" id="experience" rows="3"></textarea>
                </div>
                </div>
            </div>
            <div class="row pt-3">
              <div class="col-2 offset-5">
                <input type="hidden" name="setId" value="{{$setId}}">
                <input type="submit" value="submit" name="submit" class="btn btn-primary text-white">
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
</div>
@section('footer-script')
@include('frontend.partials.footer-script')
@endsection
@endsection
@push('styles')
<style>
  label >input,label > span {
    display:inline-block;
    vertical-align: middle;
    float: none !important;
  } 
  .form-check-label{
    margin: 0 5px;
  } 
</style>
@endpush