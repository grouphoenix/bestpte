@extends('frontend.layout.master')
@section('content')
@section('navbar')
  @include('frontend.partials.navigation')
@endsection
  <checkout-app :cart='{{auth()->user()->cart}}'></checkout-app>
@section('footer-script')
  @include('frontend.partials.footer-script')
@endsection
@endsection