<?php
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use function GuzzleHttp\json_encode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Backend Routes
Route::namespace('Admin')->prefix('bpc-admin')->group(function() {
    Route::get("/auth", 'LoginController@showLoginForm')->middleware(['guest'])->name('auth.login');
    Route::post("/auth", 'LoginController@login')->name('auth.login');
    Route::post("/logout", 'LoginController@logout')->name('auth.logout');
});


Route::namespace('Admin')->prefix('bpc-admin')->middleware(['auth','guest.admin'])->group(function(){
    Route::get("/",'DashboardController@index')->name('dashboard.admin');
    Route::prefix('user')->group(function(){
        Route::get("/lists",'UserController@index')->name('user.lists');
        Route::get("/getRoles",'UserController@getRoles')->name('user.roles');
        Route::get("/create",'UserController@createUser')->name('user.create');
        Route::post("/create",'UserController@createUser')->name('user.add');
        Route::get("/edit/{id}",'UserController@editUser')->name('user.edit');
        Route::post("/edit/{id}",'UserController@editUser')->name('user.update');
        Route::delete("/delete/{id}",'UserController@deleteUser')->name('user.delete');
        Route::delete("/deleteUsers/{array}",'UserController@deleteUsers')->name('users.delete');
        Route::get("/getUsers",'UserController@getUsers')->name('user.getUsers');
        Route::get("/search",'UserController@search')->name('user.search');
    });


    Route::prefix('role')->group(function(){
        Route::post("/create",'RoleController@createRole')->name('role.add');
        Route::get("/list",'RoleController@roleList')->name('role.list');
        Route::get("/permissions/{id}",'RoleController@permissions')->name('role.permissions');
        Route::get("/update/{id}",'RoleController@updatePermissions')->name('role.update');
    });


    Route::prefix('category')->group(function(){
        Route::get("/lists",'CategoryController@index')->name('category.lists');
        Route::post("/create",'CategoryController@addCategory');
        Route::get("/catList",'CategoryController@getCategoryList');
        Route::get("/edit/{id}",'CategoryController@editCategory');
        Route::post("/updateCategory",'CategoryController@updateCategory');
        Route::get("/delete/{id}",'CategoryController@deleteCategory');
    });

    Route::prefix('sets')->group(function(){
        Route::get("/lists",'SetsController@index')->name('set.lists');
        Route::get("/getSetLists",'SetsController@getSetLists');
        Route::get("/getCategory",'SetsController@getCategoryDropdown');
        Route::get("/create",'SetsController@createQuestion')->name('set.create');
        Route::post("/create",'SetsController@createQuestion')->name('set.add');
        Route::get("/edit/{id}",'SetsController@editQuestionSets')->name('set.edit');
        Route::post("/edit/{id}",'SetsController@editQuestionSets')->name('set.update');
        // Question section routes goes here
        Route::get("/{id}/questions/lists",'QuestionController@index')->name('set.question');
        Route::get("/questions/{id}/data",'QuestionController@getAllQuestionBySetId');
        Route::get("/questions/delete/{id}",'QuestionController@deleteQuestion');
        Route::delete("/questions/deleteQuestions/{array}",'QuestionController@deleteQuestions');
        Route::get("/questions/lists/",'QuestionController@showAllList')->name('set.question.all');
        Route::get("/{id}/questions/create",'QuestionController@createQuestion')->name('set.question.create');
        Route::get("/{id}/questions/edit/{question}",'QuestionController@editQuestion')->name('set.question.edit');
        Route::post("/questions/add/data",'QuestionController@addQuestion')->name('set.question.add');
        Route::post("/questions/update/data/{id}",'QuestionController@updateQuestion')->name('set.question.update');
        Route::delete('/deleteQuestionSets/{array}','SetsController@deleteSets');

    });
    Route::prefix('types')->group(function(){
        Route::get("/lists",'QuestionTypeController@index')->name('type.lists');
        Route::get("/lists/data",'QuestionTypeController@data')->name('type.lists.data');
        Route::post("/update/data",'QuestionTypeController@updateData')->name('type.data.update');
        Route::get("/detail/{id}",'QuestionTypeController@questionTypeById')->name('type.detail');
    });
    Route::get('settings','SettingController@siteConfig')->name('configuration');
    Route::post('updates','SettingController@updateSetting')->name('setting.update');
    Route::get('notifications','NotificationsController@index')->name('notifications');

});
// Frontend Route
Route::post('/', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/','HomeController@showLogin')->name('home');

//Register new user
Route::get('register', 'Auth\RegisterController@showRegistrationForm');
Route::post('register', 'Auth\RegisterController@register')->name('register');

//Reset Password
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

//Email verify
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

//socialite integration in bestpte
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->name('social_login');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

// Frontend Dashboard
Route::middleware(['auth'])->group(function(){
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::get('/dashboard/test/{slug}','DashboardController@showCatSets')->name('cat.questionset');
    Route::get('/pte/exam/showdetail/{id}','ExamController@showGeneralPage')->name('exam.detail');
    Route::get('/pte/exam/set/{id}/question/{page}','ExamController@showQuestionDataById')->name('exam.question.detail')->middleware('exam');

    Route::get('/exam/feedback/{setId}','ExamController@review');
    Route::post('/exam/feedback','ExamController@feedback')->name('exam.feedback');

    Route::get('/userProfile', 'ProfileController@getUserProfile');
    Route::post('/updateProfile', 'ProfileController@updateProfile');
    Route::get('/getNotifications', 'Admin\NotificationsController@getNotifications');
    Route::get('/readNotifications', 'Admin\NotificationsController@readNotifications');
    Route::get('/getNotificationsAll', 'Admin\NotificationsController@getNotificationsAll');
    Route::get('/getCountries', function(){
        return response()->json(BPC\Country::getCountriesList(),200);
    });

    /* cart */
    Route::get('/addToCart','Admin\UserController@addToCart');
    Route::get('/removeFromCart','Admin\UserController@removeFromCart');
    Route::get('/resetCart','Admin\UserController@resetCart');
    Route::get('/getCartCount','Admin\UserController@getCartCount');
    Route::get('/getCart','Admin\UserController@getCart');
    
    /* user question status tracker routes */
    Route::get('/tracker/read', 'TrackerController@read');
    Route::get('/tracker/answered', 'TrackerController@answered');

    Route::get('/paypal/create-payment', 'PaymentsController@createPayment');
    Route::get('/paypal/execute-payment', 'PaymentsController@getPaymentStatus')->name('paypal.success');

    Route::get('/paypal/cancel','PaymentsController@cancel')->name('paypal.cancel');
    Route::get('checkout', 'PaymentsController@checkout')->name('checkout');
    Route::post('/execute-payment', 'PaymentsController@createPayment')->name('execute_payment');
});


Route::get('/test/', function(){
});
