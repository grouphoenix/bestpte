<?php

use Illuminate\Http\Request;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('showdetail/{id}','ExamController@getQuestionDetail');
Route::get('showQuestion/{id}/page/{page}','ExamController@getQuestionDataById');
Route::post('saveSpeakingAnswers/{question}','AnswersController@save');
Route::post('saveReadQuestion','AnswersController@saveReadQuestionAnswer');
Route::get('getQuestionTypes/{id}','Admin\QuestionController@getQuestionTypes');
Route::get('siteVisits', 'AnalyticsController@getSiteVisitors');
Route::get('uniqueVisits', 'AnalyticsController@getUniqueVisitors');
Route::get('editQuestion/{setId}/{qid}','Admin\QuestionController@editQuestion');

Route::get('visitorSummary', 'AnalyticsController@visitorSummary');
Route::get('salesSummary', 'AnalyticsController@salesSummary');







