<?php
namespace App\Services;
use BPC\Contracts\QuestionInterface;
use Intervention\Image\Facades\Image;
use BPC\QuestionType;
use BPC\QuestionFile;
use BPC\QuestionMeta;
use BPC\QuestionAnswer;
class QuestionService 
{
    protected $question;
    public function __construct(QuestionInterface $question){
       $this->question=$question;
    }
    public function saveFiles($request,$question,$isUpdate){
        if($isUpdate): $question->files()->delete(); endif;
        if($request->hasFile('file'))
        {
            $path=$request->file('file')->store('question/file',['disk' => 'public']);
            $files=new QuestionFile();
            $files->question_id=$question->id;
            $files->file_path="/".$path;
            $files->file_type='audio';
            $files->save();
        }
        if($request->hasFile('image'))
        {
            $file=$request->file('image');
            $extension =$file->getClientOriginalExtension();
            $name =$file->getClientOriginalName();
            $mimeType=$file->getClientMimeType();
            $name = date('y-m-d') . Time() . rand(11111, 99999) . "." . $extension;
            $medium=Image::make($file->getRealPath())->resize(500,500, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
            })->save(public_path('question/img/').$name);
            $medium->destroy();
            $path='question/img/'.$name;
            $files=new QuestionFile();
            $files->question_id=$question->id;
            $files->file_path="/".$path;
            $files->file_type=$request->file('image')->extension();
            $files->save();
        }
    }

    public function saveContent($request,$query,$isUpdate)
    {
        $data=[];
        $counter=0;
        foreach($request->option as $key=>$val): $counter++;
        if(is_array($request->correct_option))
        {
          $answer=(in_array($counter,$request->correct_option)) ?  true :false;
        }else{
          $answer=($counter==$request->correct_option) ?  true :false;
        }
        $option=[];
        $option['id']=$counter;
        $option['option']= $val;
        $option['checked']=$answer;
        array_push($data,$option);
        endforeach;

        if($isUpdate){$query->metas()->delete();}
        $meta=new QuestionMeta();
        $meta->content_id=$query->id;
        $meta->meta_key=$query->qtype->slug;
        $meta->meta_value=json_encode($data);
        $meta->save();
    }
    public function saveAnswer($request,$question,$isUpdate){
      if($request->has('correct_option')):
        $counter=0;
        $data=[];
        if(is_array($request->correct_option))
        {
        foreach($request->correct_option as $key=>$val): $counter++;
            $option=[];
            $option['answer']= $val;
            array_push($data,$option);
        endforeach;
      }else{
        $option=[];
        $option['answer']= $request->correct_option;
        array_push($data,$option);
      }
      endif;
      if($isUpdate){$question->answer()->delete();}
      $meta=new QuestionAnswer;
      $meta->question_id=$question->id;
      $meta->question_type_id=$question->qtype->id;
      $meta->answer=($request->has('correct_option')) ? json_encode($data) : '';
      $meta->save();
    }

    public function saveReOrder($request,$query,$isUpdate){
      $counter=1;
      $order_option=[];
      $correct_order=[];
      foreach($request->order_option as $key=>$val):
        $option=[];
        $option['id']=$counter;
        $option['option']= $val;
        array_push($order_option,$option);
        $counter++; 
      endforeach;
      foreach($request->correct_order as $key=>$val):
        $option=[];
        $option['id']=$val;
        $option['option']=$request->correct_option[$key];
        array_push($correct_order,$option);
      endforeach;
        if($isUpdate){
          $query->metas()->delete();
          $query->answer()->delete();
        }
        $meta=new QuestionMeta();
        $meta->content_id=$query->id;
        $meta->meta_key=$query->qtype->slug;
        $meta->meta_value=json_encode($order_option);
        $meta->save();
        $answer=new QuestionAnswer;
        $answer->question_id=$query->id;
        $answer->question_type_id=$query->qtype->id;
        $answer->answer=json_encode($correct_order);
        $answer->save();
    }

    public function saveReadFillinBlank($request ,$query,$isUpdate){
      $fill_option=[];
      $correct_order=[];
      foreach($request->fill_option as $key=>$val):
        $option=[];
        $option['option']= $val;
        array_push($fill_option,$option);
      endforeach;
      foreach($request->correct_option as $key=>$val):
        $option=[];
        $option['option']=$val;
        array_push($correct_order,$option);
      endforeach;
      if($isUpdate){
        $query->metas()->delete();
        $query->answer()->delete();
      }
      $meta=new QuestionMeta();
      $meta->content_id=$query->id;
      $meta->meta_key=$query->qtype->slug;
      $meta->meta_value=json_encode($fill_option);
      $meta->save();
      $answer=new QuestionAnswer;
      $answer->question_id=$query->id;
      $answer->question_type_id=$query->qtype->id;
      $answer->answer=json_encode($correct_order);
      $answer->save();
    }

    public function saveFillinBlank($request,$query,$isUpdate){
      $fill_option=[];
      $correct_answer=[];
      foreach($request->rwfb_option as $key=>$rwfb)
      {
        $option=[];
        $option['id']=$key;
        if(is_array($rwfb))
        {
          $option['option']=$rwfb;
        }
        array_push($fill_option,$option);
      }
      $counter=1;
      foreach($request->correct_answer as $key=>$answer)
      {
       
        $list=[];
        $list['id']=$counter;
        $list['answer']=$answer;
        array_push($correct_answer,$list);
        $counter++;
      }
      if($isUpdate){
        $query->metas()->delete();
        $query->answer()->delete();
      }
      $meta=new QuestionMeta();
      $meta->content_id=$query->id;
      $meta->meta_key=$query->qtype->slug;
      $meta->meta_value=json_encode($fill_option);
      $meta->save();
      $answer=new QuestionAnswer;
      $answer->question_id=$query->id;
      $answer->question_type_id=$query->qtype->id;
      $answer->answer=json_encode($correct_answer);
      $answer->save();

    }

    public function saveListeningFillinBlank($request,$query,$isUpdate)
    {
      if($isUpdate){
        $query->answer()->delete();
      }
      $answer=new QuestionAnswer;
    
      $answer->question_id=$query->id;
      $answer->question_type_id=$query->qtype->id;
      $answer->answer=$request->fill_correct_answer;
      $answer->save();
    }

    public function saveQnAnswer($request, $query, $isUpdate){
      $answer = [];
      if($request->has('answer')){
        if($request->has('keywords')){
          $answer['keyword']= $request->keywords;
        }
        $answer['qnAnswers'] = $request->answer;
      }
        $query->answer()->updateOrCreate([
          'question_type_id' => $request->question_type,
          'answer' => json_encode($answer),
        ]);
       
      }
  }