<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use BPC\UserTracker;
use App\Http\Controllers\ScoreController;


class BegainScoring implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user_id;
    protected $set_id;

    public function __construct($user_id, $set_id)
    {
        $this->user_id = $user_id;
        $this->set_id = $set_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $score = new ScoreController($this->user_id, $this->set_id);

        $score->getScore($this->set_id);

    }
}
