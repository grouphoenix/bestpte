<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use BPC\UserAnswer;

use App\Http\Controllers\TranscriberController as TC;

class TranscribeAnswer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userAnswer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserAnswer $userAnswer)
    {
        $this->userAnswer = $userAnswer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // convert speech to text

        $transcriber = new TC();

        $text = $transcriber->transcribe($this->userAnswer->body);


        //save text in user answer

        $this->userAnswer->update([
            'text' => $text
        ]);
        //delete the audio file
        $file = $this->userAnswer->body;
        @unlink($file);

    }
}
