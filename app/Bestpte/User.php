<?php

namespace BPC;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasRoles;
use App\Traits\HasPermissions;
use App\Traits\Tracker;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, SoftDeletes, HasPermissions , Tracker ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password'',
    // ];
    protected $guarded=['created_at','updated_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsTo(Role::class,'role_id','id');
    }

    public function roles(){
        return $this->belongsToMany(Role::class)->select('user_id','role_id','name','id')->where('status',1);
    }

    public function permissions(){
        return $this->role->permissions;
    }

    public function cart(){
        return $this->belongsToMany(QuestionSets::class,'user_cart','user_id','set_id');
    }

    public function tracker(){
        return $this->hasMany(UserTracker::class)->latest()->where([['is_active',1]]);
    }

    public function trackers(){
        return $this->hasMany(UserTracker::class)->latest();
    }

    public function state($set_id){
        return $this->tracker->where('set_id',$set_id)->sortBy('created_at')->last();
    }

    public function module(){
        return $this->belongsToMany(QuestionSets::class,'user_module','user_id','set_id')->withPivot('watched');
    }

    public function answers(){
        return $this->hasMany(UserAnswer::class);
    }

    public function scores(){
        return $this->hasMany(Score::class);
    }


}
