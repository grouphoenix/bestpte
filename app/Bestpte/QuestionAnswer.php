<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
     protected $table="question_answers";
     protected $guarded=['create_at','updated_at'];
     public function questions(){
        return $this->belongsTo(Question::class,'question_id');
     }
}
