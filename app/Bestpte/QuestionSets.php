<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class QuestionSets extends Model
{
    protected $table="question_sets";
    protected $guarded=['created_at','updated_at'];
    protected $withCount=['questions'];

    public function category(){
        return  $this->belongsTo(Category::class,'cat_id');
    }

    public function questions()
    {
        return $this->hasMany(Question::class,'set_id');
    }

    public function getQuestionSetTimeAttribute($value){
    
        return   $timeInSeconds = strtotime($value) - strtotime('00:00:00');
    }

    public function tracker()
    {
        return $this->hasMany(UserTracker::class,'id','set_id');
    }

    public function user(){
        return $this->belongsToMany(User::class,'user_module','set_id','user_id')->withPivot('watched');
    }


}
