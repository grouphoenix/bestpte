<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $guarded = ['created_at','updated_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }

}
