<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class InputFields extends Model
{
     protected $table="input_fields";
     protected $guarded=['created_at','updated_at'];
}
