<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class UserTransction extends Model
{
    protected $guarded = ['created_at','updated_at'];
    protected $table = 'user_transactions';
}
