<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guarded = ['created_at','updated_at'];

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
}
