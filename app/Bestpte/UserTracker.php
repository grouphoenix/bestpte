<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class UserTracker extends Model
{

  protected $guarded = ['created_at','updated_at'];
  
  public function user(){
    return $this->belongsTo(User::class);
  }

  public function questionSet(){
    return $this->questionSer(QuestionSets::class,'set_id','');
  }
    
}
