<?php 
namespace BPC\Repo;

use BPC\Question;
use BPC\Contracts\QuestionInterface;
use Illuminate\Database\Eloquent\Model;
class QuestionRepository extends Model implements QuestionInterface
{

    private $model;
    public function __construct(Question $model){
        $this->model=$model;
    }
    public function getAllQuestion(array $params=[]){
        $question=$this->model->with('files','qtype');
        if($params)
        {
            $question->where('set_id',$params['set_id']);
        }
       return  $question->orderBy('set_id','DESC')->paginate(25);

    }
    public function getQuestionById($id){
        return $this->model->with('files','metas','answer')->findOrFail($id);
    }
    public function getQuestionBySets($id){

    }
    public function createQuestion(array $data){
         $result=$this->model->create($data);
        return $result;
    }
    public function updateQuestion(array $data,$id){
        $question=$this->model->findOrFail($id);
        if($question){
            return $question->update($data);
        }else{ 
            return false;
         }

    }
    public function deleteQuestion($id){

    }
    public function deactivateQuestion($id){
        
    }

    //Frontend Model

    public function getQuestionDataById($setId)
    {
        return $this->model
               ->with('files','metas')
               ->join('question_sets','question_sets.id','=','questions.set_id')
               ->join('categories','question_sets.cat_id','=','categories.id')
               ->join('question_types','question_types.id','=','questions.question_type_id')
               ->where('questions.set_id',$setId)
               ->select(['questions.*',
                'categories.cat_name as category_name',
                'question_types.q_type as qtype',
                'question_types.category as category',
                'question_types.slug as slug',
                'question_sets.question_set_name as set_name',
                'question_sets.question_set_time as total_time'
                ])
               ->paginate(1);
    }

}