<?php
namespace BPC\Repo;

use BPC\Contracts\UserInterface;
use Illuminate\Database\Eloquent\Model;
use BPC\User;
class UserRepository extends Model implements UserInterface
{

    private $model;
    public function __construct(User $model){
        $this->model=$model;
    }
    public function getAllUser(array $data=[]){
       return $this->model
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->when((array_key_exists('role',$data) && $data['role']!=0), function($q) use($data){
               $q->where('users.role_id',$data['role']);
            })
            ->select(['users.id','display_name','roles.name as role_name','email','users.status'])
            ->where('users.id','!=',auth()->user()->id)
            ->paginate(array_key_exists('count',$data)?$data['count']:10);
    }
    public function getUserById($id){
     return $this->model->findOrFail($id);
    }
    public function createUser(array $data){
      return $this->model->create($data);
    }
    public function updateUser(array $data,$id){
       $user=$this->model->findOrFail($id);
       return $user->update($data);
    }
    public function deleteUser($id){
        $user = $this->model->findOrFail($id);
        return $user->delete();
    }
    public function deactivateUser($id){

    }

    public function search($key){
        return $this->model->where(function($q) use($key){
                                $q->where('display_name','like','%'.$key.'%')
                                    ->orWhere('phone','like','%'.$key.'%')
                                    ->orWhere('address','like','%'.$key.'%')
                                    ->orWhere('email','like','%'.$key.'%');
                            })
                            ->join('roles', 'users.role_id', '=', 'roles.id')
                            ->select(['users.id','display_name','roles.name as role_name','email','users.status'])
                            ->where('users.status',1)
                            ->where('users.id','!=',auth()->user()->id)
                            ->paginate(10);

    }

}
