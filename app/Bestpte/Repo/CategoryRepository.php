<?php 
namespace BPC\Repo;

use BPC\Contracts\CategoryInterface;
use Illuminate\Database\Eloquent\Model;
use BPC\Category;
class CategoryRepository extends Model implements CategoryInterface
{

    private $model;
    public function __construct(Category $model){
        $this->model=$model;
    }
    public function getAllCategory(){
       return $this->model->paginate(10);
    }
    public function getCategoryDropdown(){
        return $this->model->select(['cat_name','id','parent_id'])->get();
    }
    public function getCategoryById($id){
        return $this->model->with('modules')->findOrFail($id);
    }
    public function createCategory(array $data){
        $result=$this->model->create($data);
        return $result;
    }
    public function updateCategory(array $data,$id){
        $category=$this->model->findOrFail($id);
        $category->fill($data);
        return $category;

    }
    public function deleteCategory($id){
        $category=$this->model->findOrFail($id);
        return $category->delete();
    }
    public function deactivateCategory($id){

    }
    public function getSubCategory($slug)
    {
        return $this->model
          ->with('children.question_sets.questions','question_sets.questions')
          ->select(['id','cat_slug','cat_name'])
          ->where('cat_slug',$slug)
          ->first();       
    }

}