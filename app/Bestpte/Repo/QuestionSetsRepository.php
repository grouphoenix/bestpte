<?php 
namespace BPC\Repo;

use BPC\QuestionSets;
use Illuminate\Database\Eloquent\Model;
use BPC\Contracts\QuestionSetsInterface;

class QuestionSetsRepository extends Model implements QuestionSetsInterface
{

    private $model;
    public function __construct(QuestionSets $model){
        $this->model=$model;
    }
    public function getAllSets(array $data){
        $search=!empty($data['q']) ? $data['q'] :null;
       return $this->model
                    ->when($data['cat_id']!=null, function($q) use($data){
                        $q->where('cat_id',$data['cat_id']);
                    })
                    ->when($search, function ($query, $search) {
                                    $query->where('question_set_name','LIKE' ,"%$search%")
                                        ->orwhere('question_set_type','LIKE' ,"%$search%");
                              },function ($query) {
                      $query->orderBy('id','DESC');
            })->with('category')
                
                ->paginate($data['count']);
    }
    public function getSetByCategory($cat){
        return $this->model->where('cat_id',$cat)->paginate(10);
    }
    public function getSetById($id){
        return $this->model->findOrFail($id);
    }
    public function createSet(array $data){
        return $this->model->create($data);
    }
    public function updateSet(array $data,$id){
        $Sets=$this->model->findOrFail($id);
        return ($Sets) ? $Sets->update($data) : ['error'=>'something went wrong Please check!'];
    }
    public function deleteSet($id){
        $Sets=$this->model->findOrFail($id);
        return $Sets->delete();
    }
    public function deactivateSet($id){

    }

    //Frontend section Model
    public function getsetByParams($id,$params=['*']){
        return $this->model->select($params)
               ->join('categories', 'categories.id', '=', 'question_sets.cat_id')
               ->where('question_sets.id',$id)->first();
    }
    public function getQuestionBySetsId($id){
        $sets=$this->model
             ->join('questions', 'questions.set_id', '=', 'question_sets.id')
             ->where('question_sets.id',$id)
             ->selectRaw('question_sets.id as set_id, questions.id as question_id')
             ->first();
        return $sets;
    }
}