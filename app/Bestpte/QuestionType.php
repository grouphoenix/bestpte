<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class QuestionType extends Model
{
    protected $table="question_types";
    protected $guarded=['created_at','updated_at'];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public static function getLists(){
        return self::select('*')->get();
    }

    public static function AdditionalNote(){
        return self::where('id',$id)->first()->description;
    }
    public function questions(){
        return $this->hasMany(Question::class,'question_type_id');
    }
    public function category()
    {
        return $this->belongsToMany(Category::class,'category_module','category_id','module_id')->withTimestamps();
    }


}
