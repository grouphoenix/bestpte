<?php
namespace BPC\Contracts;

interface QuestionInterface{

    public function getAllQuestion(array $params);
    public function getQuestionById($id);
    public function getQuestionBySets($id);
    public function createQuestion(array $data);
    public function updateQuestion(array $data,$id);
    public function deleteQuestion($id);
    public function deactivateQuestion($id);
}