<?php
namespace BPC\Contracts;

interface CategoryInterface{

    public function getAllCategory();
    public function getCategoryById($id);
    public function createCategory(array $data);
    public function updateCategory(array $data,$id);
    public function deleteCategory($id);
    public function deactivateCategory($id);
}