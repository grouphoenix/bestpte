<?php
namespace BPC\Contracts;

interface QuestionSetsInterface{

    public function getAllSets(array $data);
    public function getSetById($id);
    public function getSetByCategory($catId);
    public function createSet(array $data);
    public function updateSet(array $data,$id);
    public function deleteSet($id);
    public function deactivateSet($id);
}