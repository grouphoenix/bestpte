<?php
namespace BPC\Contracts;

interface UserInterface{

    public function getAllUser(array $data);
    public function getUserById($id);
    public function createUser(array $data);
    public function updateUser(array $data,$id);
    public function deleteUser($id);
    public function deactivateUser($id);
    public function search($key);
}
