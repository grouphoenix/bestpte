<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class Analytics extends Model
{
    protected $table = 'analytics';
    protected $fillable = ['visitor','visited_date','hits'];
    public $timestamps = false;


    public static function siteVisits(){
        return Analytics::get()->sum('hits');
    }


    public static function incrementSiteVisits($ip){
        $current = Analytics::firstOrCreate(['visitor'=>$ip]);
        $current->increment('hits');
    }

    public static function uniqueVisitors(){
        return Analytics::all()->count();
    }

}
