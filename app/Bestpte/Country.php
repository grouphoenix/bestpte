<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    
    protected $table="countries";
    
    public static function getCountriesList(){
         return self::select(['id','country_name'])->get();
    }
}
