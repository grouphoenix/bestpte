<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class QuestionFile extends Model
{
    protected $table='question_files';
    protected $guarded=['created_at','updated_at'];
    protected $hidden=['created_at','updated_at'];
    public function questions()
    {
        return $this->belongsTo(Question::class,'question_id');
    }
}
