<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table="roles";
    protected $fillable=['name','id','status'];
    protected $guarded = ['updated_at','created_at'];


    public function user(){
        return $this->hasMany(User::class,'role_id');
    }

    public static function  getRoles(){
      return self::select(['id','name'])->get();
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }
}
