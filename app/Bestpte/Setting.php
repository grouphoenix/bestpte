<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table="settings";
    protected $guarded=["created_at",'updated_at'];

    public static function getSetting($settingKey)
    {
        return static::getBy(['option_key'=>$settingKey]);
    }
    public static function getAllSetting()
    {
        $result = [];
        $settings = static::get();
        if ($settings) {
            foreach ($settings as $key => $row) {
                $result[$row->option_key] = $row->option_value;
            }
        }
        return $result;
    }

    public static function updateSetting($data)
    {
        $result = [
            'response_code' => 200,
            'message' => 'Your settings has been updated',
        ];
        foreach ($data as $key => $row) {
            $setting= static::getSetting($key);
            if ($setting) {
             $setting->option_value = $row;
            } else {
                $setting = new static;
                $setting->option_key = $key;
                $setting->option_value = (empty($row)) ? "" :$row ;
            }
            $setting->save();
        } 
        return $result;
    }
    public static function getBy($fields)
    {
        $obj = new static;
        $searchBy = [];
        foreach ($fields as $key => $row) {
            $current = [
                'compare' => '=',
                'value' => $row,
            ];
            $searchBy[$key] = $current;
        }
        return $obj->searchBy($searchBy);
    }
    public static function searchBy($fields)
    {
        $obj = new static;
        if ($fields && is_array($fields)) {
            foreach ($fields as $key => $row) {
                $obj = $obj->where(function ($q) use ($key, $row) {
                            $q->where($key, $row['compare'], $row['value']);
                });
            }
            return $obj->first();
        }
    }


}
