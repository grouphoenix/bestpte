<?php

namespace BPC;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

     protected $table="categories";
     protected $guarded=['created_at','updated_at'];

     public function children(){
          return $this->hasMany(Category::Class,'parent_id');
     }
     public function parent(){
          return $this->belongsTo(Category::Class,'parent_id');
     }
     public function question_sets(){
          return $this->hasMany(QuestionSets::Class,'cat_id');
     }
     public function modules()
    {
        return $this->belongsToMany(QuestionType::class,'category_module','category_id','module_id')->withTimestamps();
    }
     public static function firstCat(){
          return self::first();
     }
     public static function getParentCategory(){
          return self::select(['id','cat_slug','cat_name'])->where('parent_id',0)->get();
     }
    
}
