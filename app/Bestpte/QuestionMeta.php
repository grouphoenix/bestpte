<?php

namespace BPC;
use Illuminate\Database\Eloquent\Model;

class QuestionMeta extends Model
{
    protected $table='question_metas';
    protected $guarded=['created_at','updated_at'];
    
    public function questions(){
        return $this->belongsTo(Question::class,'content_id');
    }
}
