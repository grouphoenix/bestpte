<?php

namespace BPC;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Question extends Model
{
    protected $table="questions";
    protected $guarded=['created_at','updated_at'];
    protected $hidden=['created_at','updated_at','created_by','updated_by'];
    
    public function sets(){
        return $this->belongsTo(QuestionSets::class,'set_id');
    }
    public function metas(){
        return $this->hasOne(QuestionMeta::class,'content_id','id');
    }
    public function qtype(){
        return $this->belongsTo(QuestionType::class,'question_type_id');
    }
    public function files(){
        return $this->hasMany(QuestionFile::class,'question_id','id');
    }
    public function answer()
    {
        return $this->hasOne(QuestionAnswer::class,'question_id','id');
    }
    public function getQuestionExcerptAttribute()
    {
        return Str::words($this->question, '14');
    }
    public function getAdditionalExcerptAttribute()
    {
        return Str::words($this->additional_note, '14');
    }
}
