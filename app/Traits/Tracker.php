<?php
namespace App\Traits;
use BPC\UserTracker as UT;
use BPC\QuestionSets as QS;



trait Tracker{

  public function hasCompleted($set_id){
    $set = QS::findOrFail($set_id)->questions->count();

    if($set === 0){
      return false;
    }

    $tracker = UT::where([['set_id',$set_id],['user_id',auth()->user()->id]])->where('status','2')->count();

    if($set === $tracker){
      return true;
    } else{
      return false;
    }
  }

  public function hasAnswered($set_id,$qn_id){
    $tracker = UT::where([
                ['set_id',$set_id],
                ['question_id',$qn_id],
                ['user_id',auth()->user()->id]
            ])->where([['status','2'],['is_active',1]])->count();

    return $tracker;
  }

  public function completedSets(){
    $sets = QS::all();
    $completed = [];

    foreach($sets as $set){
      if($this->hasCompleted($set->id) === true)
      {
        $completed[] = $set->id;
      }
    }
    return $completed;
  }

}