<?php
namespace App\Traits;
use BPC\Role;
use BPC\Permission;

trait  HasPermissions{

    public function hasPermission($permission)
    {

        if(is_string($permission))
        {
            return $this->role->permissions->contains('name',$permission);
        }
        return false;
    }

    public function givePermissionToUser($permission)
    {
        return $this->role->permissions->save(Permission::whereName($permission)->firstOrFail());
    }

    public function removePermissionsFromUser($permission){
        return $this->role->permissions()->detach(Permission::whereName($permission)->firstOrFail()->id);
    }
}
