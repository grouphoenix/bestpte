<?php

namespace App\Providers;

use BPC\Repo\UserRepository;
use BPC\Contracts\UserInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProdvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('BPC\Contracts\UserInterface','BPC\Repo\UserRepository');
        $this->app->singleton('BPC\Contracts\CategoryInterface','BPC\Repo\CategoryRepository');
        $this->app->singleton('BPC\Contracts\QuestionSetsInterface','BPC\Repo\QuestionSetsRepository');
        $this->app->singleton('BPC\Contracts\QuestionInterface','BPC\Repo\QuestionRepository');
    }
}
