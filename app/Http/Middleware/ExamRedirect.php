<?php

namespace App\Http\Middleware;

use Closure;
use BPC\UserTracker;

class ExamRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $set_id = $request->route('id');
        $page = $request->route('page');

        $data = UserTracker::where([
            ['set_id',$set_id],
            ['user_id',auth()->user()->id],
            ['is_active',1]
        ])->latest()->first();
        if(!empty($data)){
            if(url($data->resume_url) == url()->full())
            {
                return $next($request);
            }
            return redirect($data->resume_url);
        }
        return $next($request);
    }
}
