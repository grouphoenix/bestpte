<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (
                !((Auth::user()->role->name == 'admin')
                ||(Auth::user()->role->name == 'teacher')
                ||(Auth::user()->role->name == 'instructor'))
             ){
                return redirect('/dashboard');
            }
        }

        return $next($request);
    }
}
