<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            // if user is admin take him to admin dashboard
            if (Auth::user()->role->name == 'admin') {
                return redirect('/bpc-admin');
            }

            // if user is corporate take him to corporate dashboard
            else if (Auth::user()->role->name == 'teacher') {
                return redirect('/bpc-admin');
            }
            // if user is vendor take him to vendor dashboard
            else if (Auth::user()->role->name == 'instructor') {
                return redirect('/bpc-admin');
            }
            // if user is vendor take him to vendor dashboard
            else if (Auth::user()->role->name == 'student') {
                return redirect('/dashboard');
            }
        }

        return $next($request);
    }
}
