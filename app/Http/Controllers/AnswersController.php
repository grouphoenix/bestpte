<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BPC\UserAnswer;
use App\Jobs\TranscribeAnswer;
class AnswersController extends Controller
{

    public function save(Request $request,$question_id){
        $this->validate($request, [
            'audio'=> 'required',
        ]);
        $file=$request->file('audio')->store('user/file',['disk' => 'public']);
        try{
           $userAnswer = UserAnswer::create([
                            'body' =>$file,
                            'user_id' => auth('api')->user()->id,
                            'question_id' => $question_id,
                            'question_type_id' =>$request->sub_section_id,
                            'time_taken' =>$request->time_taken
                        ]);
            TranscribeAnswer::dispatch($userAnswer);
            return response()->json(['message'=>'Successful'], 201);
        } catch(\Exception $e){
            return response()->json(['message'=>$e], 500);
        }

    }
    public function saveReadQuestionAnswer(Request $request)
    {
        $answer=json_encode($request->answer);
        if($request->has('wdss_answer'))
         $answer=$request->wdss_answer;
        try{
            return UserAnswer::create([
                'body' => $answer,
                'user_id' => Auth('api')->user()->id,
                'question_id' =>$request->section_id,
                'question_type_id' =>$request->sub_section_id,
                'time_taken' => $request->time_taken
            ]);
            return response()->json(['message'=>'Successful'], 200);
        } catch(\Exception $e){
            return response()->json(['message'=>$e], 500);
        }
    }
}
