<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BPC\Setting;
use BPC\User;
use BPC\Category;
use BPC\Analytics;
abstract class FrontBaseController extends Controller
{
    protected $adminCpAccess;
    protected $initCat;
    protected $loggedInUser = null;
    protected $CMSSettings;
    protected $category;
    public function __construct(){
        $this->CMSSettings=Setting::getAllSetting();
        $this->category=Category::getParentCategory();
        $this->initCat=Category::firstCat();

        if($this->CMSSettings['construction_mode']){
            abort(503);
        }
        view()->share([
            'CMSSettings'=>$this->CMSSettings,
            'initcat'=>$this->initCat,
            'parentCat'=> $this->category,
            ]);
        Analytics::incrementSiteVisits(request()->ip());
    }
    protected function _getSetting($key, $default = null)
    {
        if (isset($this->CMSSettings[$key])) {
            return $this->CMSSettings[$key];
        }
        return $default;
    }

    protected function _showConstructionMode()
    {
        if ((int)$this->_getSetting('construction_mode') == 1 && !$this->loggedInAdminUser) {
            return true;
        }

        return false;
    }
    protected function _setBodyClass($class)
    {
        view()->share([
            'bodyClass' => $class,
        ]);
    }
    protected function _setPageTitle($title)
    {
        view()->share([
            'pageTitle' => $title,
        ]);
    }

    protected function _viewFront($view, $data = [], $other=[])
    {
      
        return view('frontend.' . $view, compact('data'));
    }
    protected function _showErrorPage($errorCode = 404, $message = null)
    {
        $dis['message'] = $message;
        return response()->view('errors.' . $errorCode, $dis, $errorCode);
    }

    protected function _validateGoogleCaptcha($response)
    {
        return _validateGoogleCaptcha($this->_getSetting('google_captcha_secret_key'), $response);
    }

    protected function _sendFeedbackEmail($view, $subject, $data, $cc = [], $bcc = [])
    {
        return _sendEmail($view, $subject, $data, [
            [
            'name' => $this->_getSetting('site_title'),
            'email' => $this->_getSetting('email_receives_feedback'),
            ],
            ], $cc, $bcc);
    }

    protected function _getLoggedInAdminUser(User $adminUser)
    {
        return auth()->guard()->user();
    }

    protected function _setMetaSEO($keywords = null, $description = null, $image = null)
    {
        $data = [];
        if ($keywords) {
            $data['keywords'] = $keywords;
        } else {
            $data['keywords'] = $this->_getSetting('keywords');
        }
        if ($description) {
            $data['description'] = $description;
        } else {
            $data['description'] = $this->_getSetting('keywords');
        }
        if ($image) {
            $data['image'] = asset($image);
        } else {
            $data['image'] = asset($this->_getSetting('site_logo'));
        }
         view()->share([
            'metaSEO' => $data,
        ]);
    }
    protected function _pageview()
    {
       return $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }

}
