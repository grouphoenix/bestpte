<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class HomeController extends FrontBaseController
{

    public function __construct(){
        parent::__construct();
        $this->middleware('guest');
    }
    public function showlogin(){
        return view('frontend.login');
    }
    public function showRegister(){
        return view('frontend.register');
    }
}
