<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BPC\UserTransction as UT;
use BPC\UserModule as UM;
use BPC\QuestionSets;
use BPC\User;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use BPC\Setting;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/* paypal */

/* ./ paypal */


class PaymentsController extends Controller
{

    private $gateway;
    public function __construct()
    {
        $settings = Setting::getAllSetting();

        $this->gateway = new \Braintree\Gateway([
            'environment' => config('services.braintree.environment'),
            'merchantId' => config('services.braintree.merchantId'),
            'publicKey' => config('services.braintree.publicKey'),
            'privateKey' => config('services.braintree.privateKey')
        ]);
    }

    public function createPayment(Request $request)
    {
        $this->verifyCart();
        $response = $this->generateBody();
        if(!$response) return redirect('/dashboard')->withError('The cart is empty');
        $total = $response['total'];
        $details = $response['items'];
        $itemsInCart = $response['itemsInCart'];

        
        $nonce = $request->payment_method_nonce;
        $result = $this->gateway->transaction()->sale([
            'amount' => $total,
            'paymentMethodNonce' => $nonce,
            'customFields'=> [
                'details' => json_encode($details)
            ],
            'options' => [
                'submitForSettlement' => true
            ]
        ]);

        if ($result->success) {
            $transaction = $result->transaction;
            // store the transaction in db;
            $this->saveTransaction($transaction, $itemsInCart);
            return redirect('/dashboard')->with('success','Transaction successful. The ID is: '.$transaction->id);
        } else {
            $errorString = "";
            foreach ($result->errors->deepAll() as $error) {
                $errorString .= 'Error: ' . $error->code . ": " . $error->message . "\n";
            }

            return redirect('/dashboard')->withError('An Error occured with the message: '. $result->message);
        }
    }

    public function generateBody(){
        $this->verifyCart();
        auth()->user()->refresh();
        $cart = auth()->user()->cart;
        $total = 0;
        $itemsInCart =[];

        if(count($cart)==0){
            return false;
        }

        $items = [];
        foreach ($cart as $item) {
            $itemsInCart[] = $item->id;
            $items[] = [
                'item_id' => $item->id,
                'price' => $item->question_set_price,
                'details' => $item->question_set_name,
            ];
            $total += $item->question_set_price;
        }

        $res['items']= $items;
        $res['total']= $total;
        $res['itemsInCart'] = $itemsInCart;

        return $res;
    }

  

    public function verifyCart(){
        $inModule = auth()->user()->module;
        $module= [];
        $takenModule= [];
        foreach($inModule as $value){
            if($value->pivot['watched']){
                $takenModule[] = $value['id'];
                continue;
            }
            $module[] = $value['id'];
        }
        $inCart = auth()->user()->cart;
        $cart = [];
        $filteredCart = [];
        foreach($inCart as $value){
            $cart[] = $value['id'];
        }
        foreach($cart as $car){
            if(in_array($car,$module)){
                continue;
            } else{
                $filteredCart[] = $car;
            }
        }
        auth()->user()->cart()->sync($filteredCart);
    }

    public function saveTransaction($transaction, $itemsInCart){

        
        DB::beginTransaction();

        try{

            // insert in user module
            auth()->user()->module()->attach($itemsInCart);

            //empty cart
            auth()->user()->cart()->detach($itemsInCart);

            // save transaction

           $userTransaction = UT::create([
                'user_id' => auth()->user()->id,
                'payer_email' => isset($transaction->paypal) ? $transaction->paypal->payerEmail : auth()->user()->email,
                'payment_id'=> $transaction->id,
                'payer_id' => isset($transaction->paypal) ? $transaction->paypal->payerId : auth()->user()->id,
                'payment_method' => isset($transaction->paypal) ? 'paypal' : 'credit_card',
                'state' => $transaction->status,
                'transactions' => $transaction->customFields['details'],
                'amount' => $transaction->amount
            ]);

            DB::commit();
        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }
    }

    public function checkout(){
        $token = $this->gateway->clientToken()->generate();
       return view('frontend.braintree',[
           'token' => $token,
           'cart' => auth()->user()->cart
       ]);
    }
}
