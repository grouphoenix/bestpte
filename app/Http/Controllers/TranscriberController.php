<?php

namespace App\Http\Controllers;

use Google\Cloud\Speech\V1\SpeechClient;
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Speech\V1\RecognitionAudio;
use Google\Cloud\Speech\V1\RecognitionConfig;
use Google\Cloud\Speech\V1\RecognitionConfig\AudioEncoding;

class TranscriberController extends Controller
{
    public function __construct()
    {
        $this->storage = new StorageClient([
            'keyFile' => json_decode(file_get_contents('../includes/key.json'), true)
        ]);

    }

    public function transcribe($link=null){

        try{
            # The name of the audio file to transcribe
            // $audioFile = public_path('/audio/test.wav');
            $audioFile = public_path($link);
            # get contents of a file into a string

            $content = file_get_contents($audioFile);
            # set string as audio content
            $audio = (new RecognitionAudio())
                ->setContent($content);

            # The audio file's encoding, sample rate and language
            $config = new RecognitionConfig([
                'language_code' => 'en-US'
            ]);

            # Instantiates a client
            $client = new SpeechClient();

            # Detects speech in the audio file
            $response = $client->recognize($config, $audio);
        
            $result = $response->getResults()[0]->getAlternatives()[0]->getTranscript();
            $client->close();
            return $result;
        } catch(\Exception $e){
            throw $e; 
        }
    }
}
