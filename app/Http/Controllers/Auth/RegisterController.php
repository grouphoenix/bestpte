<?php

namespace App\Http\Controllers\Auth;

use BPC\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use BPC\Country;
use App\Notifications\NewUserRegestration;
use Illuminate\Support\Facades\Notification;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

      /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $countries=Country::getCountriesList();
        return view('frontend.register',compact('countries'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'display_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
            "country"=> ['required'],
            'phone' => ['required','min:6'],
            'address' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        return User::create([
            'display_name' => $data['display_name'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            "email"=>$data['email'],
            "role_id"=>2,
            'gender' =>'male',
            'pte_academic'=> $data['pte_academic'],
            'login_count'=>0,
            'status'=>1,
            'pte_score'=> $data['pte_score'],
            'country_id'=> $data['country'],
            'password' => bcrypt($data['password']),
        ]);
    }

      /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $this->guard()->login($user);
        $admins = User::where('role_id',1)->get();
        Notification::send($admins, new NewUserRegestration($user));
        return redirect($this->redirectPath());
    }
}
