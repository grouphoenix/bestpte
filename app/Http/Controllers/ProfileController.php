<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BPC\Contracts\UserInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    protected $user;

    public function __construct(UserInterface $user){
        $this->user=$user;
    }

    /**
     * get logged in user profile info
     */
    public function getUserProfile(){
        if(Auth::check()){
            $data = [
                'first_name' => Auth::user()->first_name,
                'last_name' => Auth::user()->last_name,
                'display_name' => Auth::user()->display_name,
                'phone' => Auth::user()->phone,
                'image' => Auth::user()->image,
                'dob' => Auth::user()->dob,
            ];
           return response()->json($data,200);
        } else{
            Session::flash('error', 'Please make sure you are logged in first');
            return redirect('/');
        }
    }

    public function updateProfile(Request $request){

        if(Auth::check()){
            $user = $this->user->getUserById(Auth::user()->id);

            $this->validate($request, [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'display_name' => 'required|string',
                'dob' => 'required|date',
                'password' => 'required_with:password_confirmed,old_password',
                'old_password' => 'required_with:password',
                'password_confirmed' => 'required_with:password',
                'phone' => 'required|numeric'
            ]);

            if($request->hasFile('image')){
                $ext = $request->file('image')->getClientOriginalExtension();
                $image_url = str_random(20) . '_' . time() . '.' . $ext;

                Image::make($request->file('image'))->resize(150, null, function($c){$c->aspectRatio();})
                                                        ->save('storage/uploads/profile/'.$image_url);
                $image_url = '/storage/uploads/profile/'.$image_url;
            } else if($request->remove){
                @unlink(ltrim(Auth::user()->image,'/'));
                $image_url = '/storage/uploads/profile/default.png';
            } else if(!$request->remove){
                $image_url = Auth::user()->image;
            }
            $data = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'display_name' => $request->display_name,
                'dob' => $request->dob,
                'phone' => $request->phone,
                'image' => $image_url
            ];

            if(!empty($request->password)){

                if(Hash::check($request->old_password, $user->password)){

                    $data['password'] = bcrypt($request->password);
                } else {
                    return response()->json(['message'=>'the given data was invalid','errors'=>['old_password'=>'the password did\'t match our records']], 422);
                }
            }

           $result = $this->user->updateUser($data,Auth::user()->id);

           if($result){
                return response()->json(['message'=>'Profile successfully updated'], 200);
           } else {
                return response()->json(['message'=>'the given data was invalid'], 500);
           }

        } else{
            Session::flash('error', 'Please make sure you are logged in first');
            return redirect('/');
        }
    }

}
