<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use BPC\Role;
use BPC\Permission;
use Illuminate\Support\Facades\Input;

class RoleController extends AdminBaseController
{


    public function createRole(Request $request){
        if(auth()->user()->can('create_roles_permissions')):
            $this->validate($request,[
                'role_name'=>'required|alpha|unique:roles,name'
            ]);
            $data=['name'=>$request->role_name];
            Role::create($data);
            return response()->json(['success'=>true,'message'=>"Role Added"]);
        else:
            abort(403);
        endif;
    }

    public function roleList(){

        if(request()->ajax()){
            if(auth()->user()->can('view_roles_permissions')):

                return Role::where('status',1)
                    ->when(auth()->user()->id!=1, function($q){
                        $q->where('id','!=',auth()->user()->role->id);
                    })
                    ->paginate();
            else:
                abort(403);
            endif;

        }
        return view('bpc-admin/roles and permissions/roles');


    }

    public function permissions(int $id){
        if(request()->ajax()){
            if(auth()->user()->can('view_roles_permissions')):
                $response['roles']  = Role::findOrFail($id)->permissions;
                $permissions  = Permission::orderBy('modules','asc')->get();
                $all = [];
                foreach($permissions as $key => $value){
                    $all[$value->modules][]=$value;
                }
                $response['all'] = $all;

                return response()->json($response, 200);
            else:
                abort(403);
            endif;
        }
        return redirect('/bpc-admin/role');
    }

    public function updatePermissions(int $id){
        if(auth()->user()->can('edit_roles_permissions')):
            $role = Role::findOrFail($id);
            $permissions = Input::get('permissions');

            $role->permissions()->sync($permissions);
            return response()->json('permissions successfully updated',200);
        else:
            abort(403);
        endif;
    }

}
