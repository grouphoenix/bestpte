<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use BPC\Contracts\QuestionInterface;
use App\Services\QuestionService;
use BPC\QuestionSets;
use DB;
use function GuzzleHttp\json_decode;

class QuestionController extends AdminBaseController
{
    protected $question;
    protected $service;
    public function __construct(QuestionInterface $question,QuestionService $service){
        parent::__construct();
        $this->question=$question;
        $this->service=$service;
    }

    public function index(Request $request,$id){
        $set=QuestionSets::findorFail($id);
        return (!empty($set)) ? view($this->admincp.".sets.questions.list"):abort(404);
    }
    public function getAllQuestionBySetId(Request $request,$id){
        $set=QuestionSets::findorFail($id);
        $addUrl=route('set.question.create',$set->id);
        $field=['set_id'=>$set->id];
        $questions=$this->question->getAllQuestion($field);
        return response()->json(['questions'=>$questions,'set'=>$set,'addurl'=>$addUrl]);
    }
    public function showAllList(){
        $questions=$this->question->getAllQuestion();
        return view($this->admincp.".sets.questions.list",compact('questions'));
    }
    public function createQuestion(Request $request,$id){
        return view($this->admincp.".sets.questions.add-edit");
    }
    public function editQuestion(Request $request,$setId,$qid){
         $set=QuestionSets::with('category.modules')->findorFail($setId);
         if($request->ajax()){
          $modules=[];
            foreach($set->category->modules as $type)
            {
              $list=[];
              $list['id']=$type->id;
              $list['qtype']=$type->q_type;
              $list['qfield']=$type->q_field;
              $list['slug']=$type->slug;
              array_push($modules,$list);
            }
           $sets=['id'=>$set->id,'question_set_name'=>$set->question_set_name]; 
           $question=$this->question->getQuestionById($qid);
           if($question)
           {
             if($question->metas && $question->metas->meta_key=='rwfb')
             {
               $readMultiple=[];
              foreach(json_decode($question->metas->meta_value) as $key=>$options)
              {
                      $list=[];
                      $list['id']=$key+1;
                      $list['options']=[];
                     if(is_array($options->option))
                     {
                       foreach($options->option as $index=>$option):
                        $list['options'][]=['id'=>$index+1,'option'=>$option];
                       endforeach; 
                     }
                     array_push($readMultiple,$list);
              }
              $question->metas->meta_value=json_encode($readMultiple);
             }
            
            return response()->json(
                [
                 'set'=>$sets,
                 'category'=>$set->category->cat_slug,
                  "question"=>$question,
                  'modules'=>$modules]);
           }else{
            return abort;
           }
         }else{
            return view($this->admincp.".sets.questions.add-edit");
         }
    }
    public function addQuestion(Request $request){
       $this->validate($request,[
      'question_type'=>'required',
      'additional_note'=>"required"]
       );
        $data=[
            'set_id'=>$request->set_id,
            'additional_note'=>$request->additional_note,
            'question'=>$request->question_name,
            'summary'=>$request->question_summary,
            'question_type_id'=>$request->question_type,
            "time_frame"=>$request->time_frame,
            'start_timer'=>$request->start_timer,
            "created_by"=>auth()->user()->id,
            "updated_by"=>auth()->user()->id,
        ];

        DB::beginTransaction();
        try {
            $query=$this->question->createQuestion($data);
            if($request->has('order_option')){
              $this->service->saveReOrder($request,$query,false);
            }elseif ($request->has('option')) {
              $this->service->saveContent($request,$query,false);
              $this->service->saveAnswer($request,$query,false);
            }elseif($request->has('fill_option')){ 
              $this->service->saveReadFillinBlank($request,$query,false);
            }elseif($request->has('rwfb_option')){ 
             $this->service->saveFillinBlank($request,$query,false);

            }elseif($request->has('fill_correct_answer')){
              $this->service->saveListeningFillinBlank($request,$query,false);
            } elseif($request->has('answer')){
              $this->service->saveQnAnswer($request,$query,false);
            }
            $this->service->saveFiles($request,$query,false);
            DB::commit();
            return response()->json(['message'=>"Questions added SuccessFully"]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e; 
        }

    }

    public function updateQuestion(Request $request,$id){
    
          $this->validate($request,[
            'question_type'=>'required',
            'additional_note'=>"required"]
             );
              $data=[
                  'set_id'=>$request->set_id,
                  'additional_note'=>$request->additional_note,
                  'question'=>$request->question_name,
                  'summary'=>$request->question_summary,
                  'question_type_id'=>$request->question_type,
                  "time_frame"=>$request->time_frame,
                  'start_timer'=>$request->start_timer,
                  "created_by"=>auth()->user()->id,
                  "updated_by"=>auth()->user()->id,
              ];
              $query=$this->question->getQuestionById($id);
              if($query)
              {
                DB::beginTransaction();
                try {
                    
                    $query->update($data);
                    $this->service->saveFiles($request,$query,true);
                    if($request->has('order_option'))
                    {
                     $this->service->saveReOrder($request,$query,true);
                    }elseif ($request->has('option')) {
                      $this->service->saveContent($request,$query,true);
                      $this->service->saveAnswer($request,$query,true);
                    }elseif($request->has('fill_option')){ 
                      $this->service->saveReadFillinBlank($request,$query,true);
                    }elseif($request->has('rwfb_option')){ 
                      $this->service->saveFillinBlank($request,$query,true);
                    }elseif($request->has('fill_correct_answer')){
                       $this->service->saveListeningFillinBlank($request,$query,true);
                    } elseif($request->has('answer')){
                      $this->service->saveQnAnswer($request,$query,true);
                    }
                    DB::commit();
                    return response()->json(['message'=>"Questions updated successfully"]);
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json(['message'=>$e]);
                }
              }else{
                return response()->json(['message'=>"Question you try to edit not found"]);
              }
          
    }
    public function deleteQuestion($id){

         $query=$this->question->getQuestionById($id);
      if($query->sets->user->where('watched',0)->count()>0){
        return ['message'=>'exists'];
      }
         if($query)
         {
           $query->files()->delete();
           $query->metas()->delete();
           $query->delete();
           return ['message'=>"Questions updated successfully"];
         }
         return response()->json(['message'=>"Something Went Wrong"],500);

    }

    public function deleteQuestions($array){
      $questions = explode(',',$array);

          foreach ($questions as $question) {
           $res = $this->deleteQuestion($question);
           if($res['message'] ==='exists'){
             return response()->json(['message'=>'the question set exits in user\'s purchase list.'], 500);
           }
          }
          return response()->json(['message'=>'success'], 200);
     
    }

    /**
     * get question types
     * @return Response::json
     */

     public function getQuestionTypes(Request $request, $id){
      $set=QuestionSets::with('category.modules')->findorFail($id);
      {
        if($request->ajax()){
          $modules=[];
          foreach($set->category->modules as $type)
          {
            $list=[];
            $list['id']=$type->id;
            $list['qtype']=$type->q_type;
            $list['qfield']=$type->q_field;
            $list['slug']=$type->slug;
            $list['category']=$type->category;
            array_push($modules,$list);
          }
          $sets=['id'=>$set->id,'question_set_name'=>$set->question_set_name];
          return response()->json(['set'=>$sets,'category'=>$set->category->cat_slug,'modules'=>$modules]);    
      } else {
        abort(404);
      }
     }

    }   
}
