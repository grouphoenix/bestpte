<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use BPC\Contracts\UserInterface;
use App\Http\Controllers\Controller;
use BPC\Role;
use BPC\Country;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class UserController extends AdminBaseController
{
    protected $user;
    public function __construct(UserInterface $user)
    {
        parent::__construct();
        $this->user=$user;
    }

    public function index(){
        if(auth()->user()->can('view_user')):
          $users=$this->user->getAllUser($data=[]);
          return view($this->admincp.'.user.list-user');
      else:
        abort(403);
      endif;
    }

    public function createUser(Request $request){
      if(auth()->user()->can('create_user')):
        if($request->method()=='GET'){
            $countries=Country::getCountriesList();
            return view($this->admincp.".user.add-edit",compact('countries'));
        }else{
             $this->validate($request,[
                'first_name'=>'required',
                'last_name'=>"required",
                "display_name"=>"required",
                "email"=>"required|email|unique:users,email",
                "address"=>"required",
                "phone"=>"required|min:10|numeric",
                "password"=>"required|min:8"
              ]);
              $data=[
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                "display_name"=>$request->display_name,
                "country_id"=>$request->country,
                "email"=>$request->email,
                "address"=>$request->address,
                "phone"=>$request->phone,
                "password"=>bcrypt($request->password),
                "pte_academic"=>$request->pte_academic,
                "pte_score"=>$request->pte_score,
                'role_id'=>$request->role_id,
                "status"=>$request->status
              ];
              $this->user->createUser($data);
              return response()->json(['success'=>true,'message'=>'User Created Successfully']);
        }
      else:
        abort(403);
      endif;
    }

    public function editUser(Request $request,$id){
      if(auth()->user()->can('edit_user')):
        if($request->method()=='GET'){
        
            $countries=Country::getCountriesList();
            $detail=$this->user->getUserById($id);
            if($request->ajax()){
                return response()->json($detail, 200);
            }else{
              return view($this->admincp.".user.add-edit");
            }
           
        }else{
             $this->validate($request,[
                'first_name'=>'required',
                'last_name'=>"required",
                "display_name"=>"required",
                "email"=>"required|email|unique:users,email,".$id,
                "address"=>"required",
                "phone"=>"required|min:10|numeric",
              ]);
              $data=[
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                "display_name"=>$request->display_name,
                "country_id"=>$request->country_id,
                "email"=>$request->email,
                "address"=>$request->address,
                "phone"=>$request->phone,
                "pte_academic"=>$request->pte_academic,
                "pte_score"=>$request->pte_score,
                'role_id'=>$request->role_id,
                "status"=>$request->status
              ];
              if($request->password!="")
              {
                $data["password"]=bcrypt($request->password);
              }
              $this->user->updateUser($data,$id);
              return response()->json(['success'=>true,'message'=>'User Update Successfully']);
        }
      else:
        abort(403);
      endif;
    }

    public function getRoles(){
      $roles=Role::getRoles();
      return $roles;
    }

    public function quickEditUser(){
    }

    /**
     * get paginate users
     */

     public function getUsers(){
       if(auth()->user()->can('view_user')):
         $data['page'] = Input::get('page') ? Input::get('page') : 1;
         $data['count'] = Input::get('count') ? Input::get('count') : 10;
         $data['role'] =  Input::get('role') ? Input::get('role') : 0;
         return $this->user->getAllUser($data);
       else:
        abort(403);
       endif;
     }

    /**
      * search user by key
      * @return App\User Paginate
    */
     public function search(){
       if(auth()->user()->can('view_user')):
          $key = Input::get('q');
          return $this->user->search($key);
       else:
        abort(403);
       endif;

     }

    /**
      * delete user
    */
    public function deleteUser($id){
      if(auth()->user()->can('delete_user')):
        if(auth()->user()->id!=$id):
          $this->user->deleteUser($id);
        endif;
      else:
        abort(403);
      endif;
    }

    public function deleteUsers($array){
      $users = explode(',',$array);
      foreach ($users as $user) {
        $this->deleteUser($user);
      }
    }
    public function addToCart(){
      if(request()->ajax()){
        $set_id = Input::get('set_id');

        //if the set is already purchased but not taken
        if(auth()->user()->module()->where('id',$set_id)->where('watched',0)->count()>0){
            Session::flash('error', 'You have already purchases this set.');
            return 'failed';
        } else if(auth()->user()->module()->where('id',$set_id)->where('watched',1)->count()>0){
            auth()->user()->module()->detach($set_id);
            $trackers = auth()->user()->trackers->where('set_id', $set_id);
            foreach($trackers as $tracker){
              $tracker->delete();
            }
        }

        if(auth()->user()->cart()->where('id',$set_id)->count()>0){
          Session::flash('error', 'Item alreay in cart.');
          return response()->json('Item alreay in cart.', 200);          
        }
        auth()->user()->cart()->attach($set_id);
        Session::flash('success', 'successfully added to cart');
        return response()->json('successfully added to cart', 200);
      } else{
        return redirect()->back();
      }
    }

    public function removeFromCart(){
      if(request()->ajax()){
        $set_id = Input::get('set_id');
        auth()->user()->cart()->detach($set_id);

        auth()->user()->refresh();

        Session::flash('success','Successfully removed');
        return auth()->user()->cart;
      } else{
        return redirect()->back();
      }
    }

    public function resetCart(){
      if(request()->ajax()){
        $set_id = [];
        auth()->user()->cart()->sync($set_id);
        Session::flash('success','Cart reseted Successfully');
        return response()->json('Cart reseted Successfully', 200);
      } else{
        return redirect()->back();
      }
    }

    public function getCart(){
      if(request()->ajax()){
        $cart = auth()->user()->cart;
        return response()->json($cart, 200);
      } else{
        return redirect()->back();
      }
    }

    public function getCartCount(){
      if(request()->ajax()){
        $cart = auth()->user()->cart->count();
        return response()->json($cart, 200);
      } else{
        return redirect()->back();
      }
    }
}
