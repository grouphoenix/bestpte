<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use BPC\QuestionType;

class QuestionTypeController extends AdminBaseController
{
    public function __construct(){
        parent::__construct();
    }
    public function index(){
        return view($this->admincp.".types.question-type");
    }
    public function data(){
        $types=$this->getQuestionType();
        return response()->json($types);
    }

    public function updateData(Request $request){
         $this->validate($request,[
             'field_type'=>"required",
             "repeator_field"=>"required_if:field_type,repeator"
         ]);
        $data=[
            "category"=>$request->category,
            "q_field"=>$request->field_type,
            "is_repeator"=>($request->is_repeator=="false") ? 0 :1,
            "repeator_field"=>($request->is_repeator=="true") ? $request->repeator_field : null
        ];
        $types=QuestionType::findOrFail($request->type_id);
        $types->update($data);
        return response()->json(['success'=>true,'message'=>"Question Type Updates"]);
    }

    public function questionTypeById($id)
    {
        $types=QuestionType::findOrFail($id);
        return response()->json($types);
    }

}
