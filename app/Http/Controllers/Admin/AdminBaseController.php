<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use BPC\QuestionType;
use BPC\Setting;
abstract class AdminBaseController extends Controller
{
    protected $admincp;
    protected $CMSSettings;
    public function __construct(){
      $this->admincp="bpc-admin";
      $this->CMSSettings=Setting::getAllSetting();
    }
    public function getQuestionType()
    {
      $types=QuestionType::getLists();
      return $types;
    }
    protected function _getSetting($key, $default = null)
    {
        if (isset($this->CMSSettings[$key])) {
            return $this->CMSSettings[$key];
        }
        return $default;
    }
    protected function _getGolbalAdditionalNote($type_id)
    {
      $types=QuestionType::AdditionalNote($type_id);
      return $types->description;
    }
}
