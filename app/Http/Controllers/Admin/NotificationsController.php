<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class NotificationsController extends Controller
{
    public function __construct()
    {
     $this->middleware('auth');
    }

    public function index(){
        return view('bpc-admin.notifications');
    }


    public function getNotifications(){
        $data['notifications'] = auth()->user()->notifications()->latest()->limit(3)->get();
        $data['count'] = auth()->user()->unreadNotifications()->count();

        return response()->json($data, 200);
    }

    public function readNotifications(){
        auth()->user()->unreadNotifications->markAsRead();
        return response()->json('success', 200);
    }

    public function getNotificationsAll(){
        $take = Input::get('take');
        $response['notifications'] = auth()->user()->notifications()->latest()->skip(0)->take($take)->get();
        $response['count'] = auth()->user()->notifications()->count();

        return response()->json($response,200);
    }
}
