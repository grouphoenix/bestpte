<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use BPC\Setting;
class SettingController extends Controller
{
    private $setting;
    private $languages;
    public function __construct(Setting $setting)
    {
        $this->setting=$setting;
    }
    public function siteConfig()
    {
        $settings=$this->setting->getAllSetting();
        return view("bpc-admin.setting",compact('settings'));
    }
    public function updateSetting(Request $request){
       
        $data=$request->except(['_token','update']);
        if($request->hasFile('site_logo'))
        {
            $site_logo=$this->setting->getSetting('site_logo');
            if($site_logo)
            {
                if(file_exists(public_path($site_logo->option_value)))
                {
                    @unlink(public_path($site_logo->option_value));
                }
            }            $path=$request->file('site_logo')->store('img',['disk' => 'public']);

            $data['site_logo']="/".$path;
        }
        if($request->hasFile('site_login_banner'))
        {
            $site_logo=$this->setting->getSetting('site_login_banner');
            if($site_logo)
            {
                if(file_exists(public_path($site_logo->option_value)))
                {
                    @unlink(public_path($site_logo->option_value));
                }
            }
            $path=$request->file('site_login_banner')->store('img',['disk' => 'public']);
            $data['site_login_banner']="/".$path;
        }
        if($request->hasFile('site_register_banner'))
        {
            $site_logo=$this->setting->getSetting('site_register_banner');
            if($site_logo)
            {
                if(file_exists(public_path($site_logo->option_value)))
                {
                    @unlink(public_path($site_logo->option_value));
                }
            }
            $path=$request->file('site_register_banner')->store('img',['disk' => 'public']);
            $data['site_register_banner']="/".$path;
        }
        $data['construction_mode']=($request->construction_mode) ? 1 :0;
        $this->setting->updateSetting($data);
        return redirect()->back();
    }

    
}
