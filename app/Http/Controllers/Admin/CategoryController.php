<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use BPC\Contracts\CategoryInterface;
class CategoryController extends AdminBaseController
{
    public function __construct(CategoryInterface $category)
    {
        parent::__construct();
        $this->category=$category;
    }
    public function index(){
        return view($this->admincp.'.category.index');
    }

    public function getCategoryList(){
        $categories=$this->category->getAllCategory();
        $options=buildTree($this->category->getCategoryDropdown()->toArray());
        $types=$this->getQuestionType();
        $qtypes="";
        foreach($types as $type):
          $qtypes.="<option value=".$type->id." >$type->q_type</option>";
        endforeach;
        return response()->json(compact('categories','options','qtypes'));
    }

    public function editCategory($id)
    {
        $detail=$this->category->getCategoryById($id);
        $categories=buildTree($this->category->getCategoryDropdown()->toArray());
        $types=$this->getQuestionType();
        $qtypes="";
        foreach($types as $type):
          $selected=(in_array_r($type->id,$detail->modules->toArray())) ? "selected" : "";
          $qtypes.="<option value=".$type->id." $selected >$type->q_type</option>";
        endforeach;
        return response()->json(compact('categories','detail','qtypes'));
        //return view($this->admincp.'.category.edit',compact('categories','detail','types'));
    }
    public function addCategory(Request $request)
    {
        $this->validate($request,['category'=>'required']);
        $data=[
        'cat_name'=>$request->category,
        'cat_slug'=>$request->slug,
        'parent_id'=>$request->parent_id,
        'is_active'=>$request->status
         ];
        $result=$this->category->createCategory($data);
        $result->modules()->attach($request->modules_id);
        return response()->json(['success'=>true,'message'=>'Category Created successfully']);

    }
    public function updateCategory(Request $request){
        $this->validate($request,['category'=>'required']);
        $id=$request->id;
        $data=[
        'cat_name'=>$request->category,
        'cat_slug'=>$request->slug,
        'parent_id'=>$request->parent_id,
        'is_active'=>$request->status
         ];
        $result=$this->category->updateCategory($data,$id);
        $result->modules()->sync($request->modules_id);
        return response()->json(['success'=>true,'message'=>'Category Successfully Update']);
    }
    public function deleteCategory($id)
    {
        $result=$this->category->deleteCategory($id);
        if($result)
        {
            return response()->json(['success'=>true,'message'=>'Category Successfully Deleted']);
        }else{
            return response()->json(['success'=>false,'message'=>'Category Try to delete doesn\' Exist']);
        }
    }
}
