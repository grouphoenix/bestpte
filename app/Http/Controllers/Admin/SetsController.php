<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use BPC\Contracts\CategoryInterface;
use BPC\Contracts\QuestionSetsInterface;
use BPC\QuestionSets as QS;

class SetsController extends AdminBaseController
{
    protected $category;
    protected $sets;
    public function __construct(CategoryInterface $category,QuestionSetsInterface $sets){
        parent::__construct();
        $this->category=$category;
        $this->sets=$sets;
    }

    public function index(){
     $options=buildTree($this->category->getCategoryDropdown()->toArray());
      return view($this->admincp.".sets.index",compact('options'));
    }
    public function getSetLists(Request $request){
        $fields=[
            'q'=>$request->q,
            'count' => (int) $request->count,
            'cat_id' => (int) $request->cat_id
        ];
        $sets=$this->sets->getAllSets($fields);
        return response()->json($sets);
    }
    public function getCategoryDropdown(Request $request){
        if($request->ajax())
        {
            $options=buildTree($this->category->getCategoryDropdown()->toArray());
            return categorydropdownListing($options);
        }
        return abort(404);
      
    }
    public function createQuestion(Request $request){

            $this->validate($request,['set_name'=>'required','set_type'=>'required','set_category'=>'required']);
            $data=[
            'cat_id'=>$request->set_category,
            'question_set_name'=>$request->set_name,
            'question_set_type'=>$request->set_type,
            'question_set_price'=>$request->set_price,
            'question_set_time'=>$request->set_time
            ];
            $this->sets->createSet($data);
            return response()->json(['success'=>true,'message'=>'Sets Created Successfully']);
    }

    public function editQuestionSets(Request $req,$id)
    {
        if($req->method()=='GET')
        {
             return $this->sets->getSetById($id);
        }else{
            $this->validate($req,['set_name'=>'required','set_type'=>'required']);
            $data=[
            'cat_id'=>$req->set_category,
            'question_set_name'=>$req->set_name,
            'question_set_type'=>$req->set_type,
            'question_set_price'=>$req->set_price,
            'question_set_time'=>$req->set_time
            ];
           $updates=$this->sets->updateSet($data,$id);
           if(!$updates)
           {
            return response()->json(['success'=>false,'message'=>$updates['error']]);
           }
           return response()->json(['success'=>true,'message'=>'Sets Updated Successfully']);
         
        }

    }

    public function deleteSets($array){
        $sets = explode(',',$array);
        try {
            foreach($sets as $set){
              $this->destroy($set);
            }
            return response()->json(['message'=>'success'], 200);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function destroy($id){
        $set = QS::findOrfail($id);
        if($set){
            
           $users = $set->user->where('watched',0);
           if($users->count() >0){
               return 'unable to delete, the question has current customers';
           }
           if($set->question){
                $set->question()->delete();
           }
            $set->delete();
            return response()->json(['message'=>'success'], 200);
        } else {
            return response()->json(['message'=>'failed'], 500);
        }
    }
}
