<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BPC\QuestionSets;
use BPC\QuestionType;
use BPC\Score;
use function GuzzleHttp\json_decode;
use BPC\User;

class ScoreController extends Controller
{
    protected $qnAns;
    protected $user_id;
    protected $set_id;
    protected $user;

    public function __construct($user_id, $set_id){
        $this->user_id = $user_id;
        $this->set_id = $set_id;
        $this->user = User::findOrFail($user_id);
    }

    public function getScore($id)
    {

        $scores = [];
        $score = 0;
        $total = 0;
        $this->qnAns = $this->mapQuestionAnswer($id);
        foreach ($this->qnAns as $index => $qn) {
            $scores[] = $this->mark($qn);
            $score += (float)$scores[$index][0];
            $total += (float)$scores[$index][1];
        }

        $scores['score'] = $score;
        $scores['total'] = $total;
        $scores['pteScore'] = number_format((float)($score / $total) * 90, 2, '.', '') . ' / 90';

        Score::firstOrCreate([
            'user_id' => $this->user->id,
            'set_id' =>  $id,
            'total_marks' => $scores['total'],
            'scored_marks' => $scores['score'],
        ]);
    }

    /**
     * calculate marks of the given question
     */
    public function mark($qn)
    {
        $type = QuestionType::findOrFail($qn['question_type']);
        if ($type->category === 'reading') {
            return $this->markReading($qn);
        } else if ($type->category === 'speaking') {
            return $this->markSpeaking($qn);
        } else if ($type->category === 'writting') {
            return $this->markWritting($qn);
        } else if ($type->category === 'listening') {
            return $this->markListening($qn);
        }
    }

    public function markReading($qn)
    {
        $qnType = $qn['question_type'];
        switch ($qnType) {
            case '8':
                return $this->mccsa($qn);
                break;

            case '9':
                return $this->mccma($qn);
                break;

            case '10':
                return $this->rop($qn);
                break;

            case '11':
                return $this->rfb($qn);
                break;

            case '12':
                return $this->rwfb($qn);
                break;
        }
    }
    public function markListening($qn)
    {
        $qnType = $qn['question_type'];
        switch ($qnType) {
            case '13':
                return $qnType;
                break;

            case '14':
                $qn['answer'] = json_encode(explode(',', $qn['answer']));
                return $this->flnb($qn);
                break;

            case '15':
                return $this->hcs($qn);
                break;

            case '16':
                return $this->smw($qn);
                break;

            case '17':
                return $this->hiw($qn);
                break;

            case '18':
                return $this->wfd($qn);
                break;

            case '19':
                return $this->mccsa($qn);
                break;

            case '20':
                return $this->mccma($qn);
                break;

            default:
                return 'none';
                break;
        }
    }
    public function markSpeaking($qn)
    { }
    public function markWritting($qn)
    { }



    /**
     * scoring by question type
     */

    # multiple choice, choose single answer
    public function mccsa($qn)
    {
        $answer = (int)json_decode($qn['answer'])[0]->answer;
        $userAnswer = (int)json_decode($qn['user_answer']);
        if ($answer === $userAnswer) {
            return array(1, 1);
        } else {
            return array(0, 1);
        }
    }

    # multiple choice, choose multiple answer
    public function mccma($qn)
    {
        $score = 0;
        $answers = json_decode($qn['answer']);
        $userAnswer = json_decode($qn['user_answer']);
        $total = count($answers);
        foreach ($answers as $index => $answer) {
            if (in_array($answer->answer, $userAnswer)) {
                $score++;
            } else {
                if (!$score == 0)
                    $score--;
            }
        }
        return array($score, $total);
    }

    # re-order paragraph
    public function rop($qn)
    {
        $answers = json_decode($qn['answer']);
        $userAnswer = json_decode(json_decode($qn['user_answer']));
        $last = '';
        $score = 0;
        $total = 0;

        foreach ($answers as $index => $answer) {
            if (count($userAnswer) <= $index) {
                break;
            }
            if ($index == 0) {
                $last = $answer->id;
                continue;
            } else {
                $total++;
                if ($last == $userAnswer[$index - 1] && $answer->id == $userAnswer[$index]) {
                    $score++;
                }
                $last = $answer->id;
            }
        }
        return array($score, $total);
    }

    # fill in the blanks
    public function rfb($qn)
    {
        $score = 0;
        $answers = json_decode($qn['answer']);
        $userAnswer = json_decode($qn['user_answer']);
        $total = 0;
        foreach ($answers as $index => $answer) {
            $total++;
            if ($answer->option === $userAnswer[$index]) {
                $score++;
            }
        }
        return array($score, $total);
    }

    # read write fill in the blanks
    public function rwfb($qn)
    {
        $score = 0;
        $total = 0;
        $answers = json_decode($qn['answer']);
        $userAnswer = json_decode($qn['user_answer']);

        foreach ($answers as $index => $answer) {
            $total++;
            if ($answer->answer === $userAnswer[$index]) {
                $score++;
            }
        }
        return array($score, $total);
    }

    # fill in the blanks
    public function flnb($qn)
    {
        $answers = json_decode($qn['answer']);
        $userAnswer = json_decode($qn['user_answer']);
        $score = 0;
        $total = 0;

        foreach ($answers as $index => $answer) {
            $total++;
            if ($answer == $userAnswer[$index]) {
                $score++;
            }
        }
        return array($score, $total);
    }

    #highlight current summary
    public function hcs($qn)
    {
        $answer = (int)json_decode($qn['answer'])[0]->answer;
        $userAnswer = (int)json_decode($qn['user_answer'])[0];

        if ($userAnswer === $answer) {
            return array(1, 1);
        } else {
            return array(0, 1);
        }
    }

    #select missing word
    public function smw($qn)
    {
        $answer = (int)json_decode($qn['answer'])[0]->answer;
        $userAnswer = (int)json_decode($qn['user_answer'])[0];

        if ($userAnswer === $answer) {
            return array(1, 1);
        } else {
            return array(0, 1);
        }
    }

    # highlight incorrect word
    public function hiw($qn)
    {
        $answers = explode(',', $qn['answer']);
        $userAnswer = json_decode($qn['user_answer']);
        $score = 0;
        $total = count($answers);

        foreach ($userAnswer as $answer) {
            $answer = preg_replace('/[.,]/', '', trim($answer));

            if (in_array($answer, $answers, false)) {
                $score++;
            }
        }
        return array($score, $total);
    }

    # write from dictation
    public function wfd($qn)
    {
        $answers = explode(' ', preg_replace(
            '/[.,]/',
            '',
            trim(json_decode($qn['answer'])->qnAnswers)
        ));
        $userAnswer = explode(' ', preg_replace('/[.,]/', '', trim($qn['user_answer'])));
        $score = 0;
        $total = count($answers);

        foreach ($answers as $answer) {
            if (in_array($answer, $userAnswer)) {
                $score++;
            } else {
                // $score--;
            }
        }

        return array($score, $total);
    }

    /**
     * map set's question, question's answer with auth user's answer
     */
    public function mapQuestionAnswer($id)
    {
        $res = [];
        $questions = QuestionSets::find($id)->questions;

        foreach ($questions as $question) {
            $qp['question'] = $question->question;
            $qp['question_type'] = $question->question_type_id;
            if ($question->question_type_id == 1) {
                $qp['answer'] = $question->question;
            } else {
                $qp['answer'] = $question->answer['answer'];
            }
            $qp['user_answer'] = $this->user->answers->where('question_id', $question->id)->first()->body;
            $qp['user_ans_text'] = $this->user->answers->where('question_id', $question->id)->first()->text;

            $res[] = $qp;
        }
        return $res;
    }
}
