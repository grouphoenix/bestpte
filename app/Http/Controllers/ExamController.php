<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use BPC\Contracts\QuestionInterface;
use BPC\Contracts\QuestionSetsInterface;
use App\Feedback;
use BPC\UserTracker;
use BPC\UserAnswer;
use Illuminate\Support\Facades\Auth;

class ExamController extends FrontBaseController
{
    protected $question_set;
    protected $question;
    public function __construct(QuestionSetsInterface $set,QuestionInterface $question){
        parent::__construct();
        $this->question_set=$set;
        $this->question=$question;
    }

    public function showGeneralPage(Request $request,$id){
        return $this->_viewFront("generalpage",$id);
    }

    public function showQuestionDataById($set_id,$page){
        return $this->_viewFront("exam",['set_id'=>$set_id,'page'=>$page]);
    }
    //api section 
    public function getQuestionDetail($id){
        $questions=$this->question_set->getQuestionBySetsId($id);
        return response()->json($questions);
    }
    public function getQuestionDataById(Request $request,$set_id,$page){
            $time_taken = UserAnswer::whereHas('question', function($q) use($set_id){
                $q->where('set_id',$set_id)->where('user_id',Auth('api')->user()->id);
            })
            ->select('time_taken')->latest()->first();

            $request->merge(['page'=>$page]);
            $sets=$this->question_set->getsetByParams($set_id,['question_set_time','categories.cat_slug as slug']);
            $questions=$this->question->getQuestionDataById($set_id);
            $nextpage=nextPageNumber($questions->nextPageUrl());

            return response()->json(['data'=>$questions->items()[0],
                'totalPages'=>$questions->total(),
                'nextpage'=>$nextpage,
                'set_id'=>$set_id,
                'total_time'=>$sets->question_set_time,
                'time_taken' => $time_taken['time_taken'],
                'category'=>$questions->items()[0]->category]);
    }


    /*feedback */

    public function feedback(Request $request){
        $this->validate($request,[
            'quality' => 'required',
            'difficulty' => 'required',
            'setId' => 'required',
            'experience' => 'required'
        ]);

        Feedback::create([
            'quality' => $request->quality,
            'difficulty' => $request->difficulty,
            'set_id' => $request->setId,
            'user_id' => auth()->user()->id,
            'review' => $request->experience
        ]);

        return redirect('/dashboard');
    }

    public function review($setId){

        // check if user has completed the set
        $status = UserTracker::where('user_id',auth()->user()->id)->where('set_id',$setId)->latest()->limit(1)->first();
        if($status->status!=3){
            abort(404);
        }
        //check if user has already given feedback
        $history = Feedback::where('user_id',auth()->user()->id)->where('set_id',$setId)->count();
        if($history>0){
            abort(404);
        }

        return view('frontend.feedback')->withSetId($setId);
    }

   
   
}