<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BPC\UserTracker as UC;
use Illuminate\Support\Facades\Input;
use DB;
use BPC\QuestionSets;
use App\Jobs\BegainScoring;

class TrackerController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * add read tracker for user exam
     */
    public function read(){
        if(request()->ajax()){
            $set_id = Input::get('set_id');
            $qid = Input::get('qid');
           $old = UC::where([['set_id',$set_id],['question_id',$qid],['status','1'],['user_id',auth()->user()->id]])->count();
           if($old>0){
               return response()->json('already done.',200);
            } else{

            UC::create([
                'user_id' => auth()->user()->id,
                'set_id' => Input::get('set_id'),
                'question_id' => Input::get('qid'),
                'resume_url' => Input::get('url'),
                'status' => 1,
                'is_active' => 1
            ]);
            return response()->json('ok', 200);
            }
        } else {
        return redirect('/dashboard');
        }

    }


    /**
     * add answered tracker for user exam
     */
    public function answered(){

        if(request()->ajax()){
            $set_id = Input::get('set_id');
            $question_id = Input::get('qid');
            $end = Input::get('end');

           DB::beginTransaction();
           try{ 
            $old = UC::where([
                                    ['set_id',$set_id],
                                    ['question_id',$question_id],
                                    ['is_active',1],
                                    ['status','1'],
                                    ['user_id',auth()->user()->id]
                                ])
                                ->latest()
                                ->first();
            $old->is_active = 0;

            $uc = Uc::create([
                    'user_id' => auth()->user()->id,
                    'set_id' => Input::get('set_id'),
                    'question_id' => Input::get('qid'),
                    'resume_url' => Input::get('url'),
                    'status' => $end =='true' ? 3 : 2,
                    'is_active' => 1
                    ]);
                if($end =='true'){
                    
                    // Begain Scoring
                    BegainScoring::dispatch(auth()->user()->id, $set_id);

                    $qs = QuestionSets::findOrFail($set_id);
                    if($qs->question_set_type=='paid'){
                        auth()->user()->module()->updateExistingPivot($set_id,['watched'=>1]);
                    }
                }

                $old->save();
                DB::commit();
                return response()->json('ok', 200);
            } catch(\Exception $e){
                DB::rollback();
                return 'failed';
            }



        } else {
        return redirect('/dashboard');
        }
    }
}
