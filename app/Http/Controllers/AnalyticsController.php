<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BPC\Analytics;
use BPC\UserTransction;
use Illuminate\Support\Carbon;

class AnalyticsController extends Controller
{
    public function getSiteVisitors(){
        return Analytics::siteVisits();
    }

    public function getUniqueVisitors(){
        return Analytics::uniqueVisitors();
    }

    public function getVisitsToday(){
        return Analytics::whereDate('visited_date','>=', Carbon::today()->format('Y-m-d H:i'))->get()->sum('hits');
    }

    public function getVisitsThisWeek(){
        return Analytics::whereDate('visited_date','>=', Carbon::now()->startOfWeek()->format('Y-m-d H:i'))->get()->sum('hits');        
    }

    public function visitorSummary(){
        return [
            'siteVisits' => $this->getSiteVisitors(),
            'uniqueVisits' => $this->getUniqueVisitors(),
            'dailyVisits' => $this->getVisitsToday(),
            'weeklyVisits' => $this->getVisitsThisWeek(),
        ];
    }

    public function totalSales(){
      return  UserTransction::all()->sum('amount');
    }

    public function lastMonthGrowth(){
        $last_month = UserTransction::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->get()->sum('amount');
        $this_month = UserTransction::whereMonth('created_at','=',Carbon::now()->month)->get()->sum('amount');
        if($last_month==0) return 0;

        return (($this_month - $last_month)/$last_month) * 100;
    }
    public function dailySales(){
        $dataCollection = UserTransction::orderBy('created_at','asc')->get(['id','created_at','amount'])->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('Y-m-d');
      });
      $dataCollection= $dataCollection->toArray();
      $datas = [];
      foreach ($dataCollection as $data) {
          $sales = 0;
          $date = null;
          foreach($data as $key => $val){
                $sales += $val['amount'];
                $date = $val['created_at'];
          }

          $daily['t'] = strtotime($date);
          $daily['y'] = $sales;
          $datas[] = $daily;
      }

      return $datas;
    }

    public function salesSummary(){
        return [
            'totalSales' => $this->totalSales(),
            'salesGrowth' => $this->lastMonthGrowth(),
            'dailySales' => $this->dailySales()
        ];
    }


}
