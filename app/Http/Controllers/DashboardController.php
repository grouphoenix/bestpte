<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BPC\Contracts\CategoryInterface;
class DashboardController extends FrontBaseController
{
  protected $category;
  public function __construct(CategoryInterface $category){

     parent::__construct();
     $this->category=$category;
  }
  public function index(Request $request)
  {
    if (!$request->session()->has('pte')) {
      $request->session()->put('pte','yes');
      return view('frontend.choose-category');
    }
    return $this->showCatSets($slug=$this->initCat?$this->initCat->cat_slug:'' );
  }

  public function showCatSets($slug){
    $subcategories=$this->category->getSubCategory($slug);
    $user_cart = auth()->user()->cart;
    $user_module = auth()->user()->module;
    
    $cart = [];
    $module = [];
    foreach($user_cart as $value){
      $cart[] = $value['id'];
    }

    foreach($user_module as $value){
      if($value->pivot->watched==0)
        $module[] = $value['id'];
    }

    if($subcategories)
    {
      if($subcategories->children->isEmpty())
      {
        return  view('frontend.mockup',compact('subcategories','cart'));
      }
      return view('frontend.dashboard',compact('subcategories','cart','module'));
    }else{
      abort(404);
    }
  }


}
