<?php 

function currentActiveLink($target){
    $pos = strpos(request()->path(),$target);
     if($pos!==false)
     {
        echo 'menu-open active';
     }

}
function currentViewBlock($target){
    $pos = strpos(request()->path(),$target);
     if($pos!==false)
     {
        echo "style='display:block'";
     }
}
function navActive($parent,$target=null){

$parent= strpos(request()->path(),$parent);
$pos = strpos(request()->path(),$target);
    if($pos!==false && $parent!==false)
    {
    echo "active";
    }
  
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
    return false;
}

function getStatus($status)
{
    if(!empty($status)){
        switch ($status) {
            case '0':
            return "<label class='label bg-danger'>Deactive</label>";
            break;
            default:
            return "<label class='label bg-success'>Active</label>";
            break;
        }
    }else{
       return "<label class='label label-danger'>Out Of Stock</label>";
   }
}

function splitString($value){
    return str_split($value);
}

function categorydropdownListing($data,$selected=null)
{
    if(is_array($data))
    {
        $html="";
        foreach($data as $cat)
        {  
            $selected=($cat['parent_id']==$selected) ? "selected " :"";
            $html.="<option value='{$cat['id']}' {$selected}>{$cat['cat_name']}</option>".PHP_EOL;
            if(array_key_exists('children',$cat)){
                foreach($cat['children'] as $children)
                {
                 $selected=($children['parent_id']==$selected) ? "selected " :"";
                 $html.="<option value='{$children['id']}' {$selected}>-{$children['cat_name']}</option>".PHP_EOL;
                }
            }
        }
        return $html; 
    }
    return false;
}

function buildTree(array $elements, $parentId = 0) {
    $branch = array();
    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
}
function getFieldsDropdown(){
    $data=['text'=>"Text",
        'textarea'=>"Textarea",
        'select'=>"Select",
        'audio'=>"Audio",
        'img'=>"image",
        "checkbox"=>"Checkbox",
        "radio"=>"radio",
        "repeator"=>"Repeator"
    
    ];
        $html="";
        foreach($data as $key=>$value)
        {
         $html.="<option value='{$key}'>{$value}</option>".PHP_EOL;
        }
        return $html;
}

function convertToTime($value){
   return  $time = date('H:i',strtotime($value));
}

function nextPageNumber($url)
{
    $pos = strpos($url,"=");
    if($pos!=false)
    {
        $split=explode("=",$url);
        return $split[1];
    }
    return "";

}
 function canNavigate($module){
    //  return auth()->user()->permissions()->where('modules',$module)->groupBy('modules');
    $result = auth()->user()->permissions()->where('modules',$module)->groupBy('modules');
    if(auth()->user()->role->name=='admin')
    {
        return true;
    }
    return  $result->count() > 0 ? true : false;
}

