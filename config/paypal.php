<?php
/**
 * PayPal Setting & API Credentials
 * Created by Raza Mehdi <srmk@outlook.com>.
 */


return [
    'client_id' => env('PAYPAL_CLIENT_ID',''),
    'secret' => env('PAYPAL_SECRET',''),
    'settings' => array(
        'mode' => env('PAYPAL_MODE','sandbox'),
        'http.ConnectionTimeOut' => 300,
        'log.logEnabled' => true,
        'log.FileName' => storage_path().'/logs/paypal.log',
        'log.logLevel' => 'ERROR'
    ),
    ];
