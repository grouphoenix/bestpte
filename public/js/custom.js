jQuery(document).ready(function() {

    ManageFooterPosition = function() {
        windowHeight = parseInt(jQuery(window).height());
        contentHeight = parseInt(jQuery('.wrapper').outerHeight());
        if (contentHeight < windowHeight) {
            jQuery('footer').addClass('fixFooter');
        } else {
            jQuery('footer').removeClass('fixFooter')
        }
    }
    ManageFooterPosition();
    jQuery(window).bind('resize orientationchange', function() {
        ManageFooterPosition();
    })
})